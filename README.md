# Yutani
A simple wayland compositor.

# Compile And Install

To compile:
```
make
```

To compile and install:
```
make install
```

# More protocols
If you find that you need an extra protocol to use some client make an issue.

# dwm compliance
If you think we aren't compliant with dwm's behavior in some way. Then make an issue.

# Contribute
If you want to help out see the [todo](#TODO) section for tasks that need to be worked on.

# TODO
+ [x] keys aren't getting forwarded to the client. This might have something to do with the focusing of clients
or the fact that we don't handle pointer motion.
+ [x] Use the server decorations protocol to stop clients from making their own decorations.
+ [x] Make tags actually work.
+ [x] Remove the state struct and move pretty much everything into main.c. Expand the state struct into globals at the top to make things simple
+ [x] Create the bar. Don't make a status thing just yet, we'll work on it. Status input is either going to be a socket/fifo file or through a protocol.
+ [x] Support the ability of clients to float. With keybinds and stuff.
+ [x] Add support for the xdg activation protocol.
+ [x] Add a vertical split tiling layout with the symbol '|||' which splits the clients evenly vertically
+ [x] Finish keybinds from dwm and consider adding a fullscreen keybind from dwl
+ [x] Consider moving all of the user.h stuff to main.c then just make config.h include into main.c in the same way dwm does.
    + [x] Organize all of the functions into alphabetical order
+ [x] Get pointer stuff working better like motion handling resizing and moving and button binds.
+ [x] Properly rename client based functions and stuff so that it's understood whether a function is working on the opaque struct client or the implemented client type (ie. toplevel, or layered).
+ [x] Fully support the xdg shell protocol and fullscreen protocol
+ [x] layer clients
+ [x] x11 clients
+ [x] Locking
+ [x] Idle inhibit
+ [x] Gamma control
+ [x] Make it so that we don't have multiple levels of indirection going on with struct client and struct window
instead just make it so that the window type has WINDOW_TOPLEVEL, WINDOW_LAYERED, and WINDOW_BAR, then just have struct toplevel
have it's own encompasing type enum for differentiating between x11 and xdg. A lot of this is already done just fit it into functions
and structures.
+ [x] seat stuff
+ [x] idle notifier
+ [x] virtual keyboard and pointer
+ [x] exclusive clients
+ [x] fix focus_stack
+ [x] Do some testing to see why some apps are messed up looking upon start with yutani but not dwl (e.g. firefox).
+ [x] add monitor rules
+ [x] fix pertags to be more like the suckless patch, see [pertag patch](https://dwm.suckless.org/patches/pertag/dwm-pertag-20200914-61bb8b2.diff) for more details.
+ [x] change monitor_layers_arrange to take in a wlr_box pointer and return the new usable space. If null then compare and arrange.
+ [x] Add a geometry box for the bar.
+ [x] Make it so that when doing monitor_layers_arrange that when the toplevel_space is different from usable
we manage the different of the bar geometry, in otherwords make it so that toplevel_space no matter the new usable geometry doesn't go over bar geometry if enabled.
+ [x] Remove show_bar and just make everything check for bar.scene.node.enabled.
    + [x] This means that pertag will use it do set the show_bar array.
+ [x] Move all the toplevel lists into struct monitor just to make things easier NOTE: This may need more testing
+ [x] Make it so that when we disable a monitor via output manager that we "close" the monitor and move all of it's toplevels elsewhere. NOTE: Needs testing.
+ [x] output manager NOTE: Needs testing
+ [x] Add support for the keyboard shortcuts protocol
+ [x] Add support for the relative pointer and pointer constraints protocols
    + [x] Look at the protocols for this.
    + [x] Look at the dwl and sway implementation and try to figure out what's going on it them.
+ [ ] Do testing to make sure things work.
    + [ ] output management and monitors turning on and off.
    There are problems with turning monitors off and on again. Things will often freeze and depending on the drivers and drm version and hardware the details of this
    seem to be different. I am unable to figure out what the problem might be or how to fix it on my end, or even if it is a problem on my end.
    An issue request might need to be made in the wlroots issue tracker and further testing is needed.
    + [ ] locking
    + [ ] virtual pointers and keyboards
+ [ ] Fix the scale of drawing the bar, this means scaling pretty much everything that is dependent on a strict height or whatever, like font height and stuff like that. I don't really know the full extent of this though.
+ [ ] Find out how dwm/xorg chooses which monitor to place a window on when monitors are turned off then turned on at different intervals.
+ [x] Test dwm's pointer focus stuff and make sure we're compliant.
+ [x] Test keyrepeat with keybinds in dwm then implement them here.
+ [x] Add config stuff for libinput devices specifically for the pointer.
+ [x] Change up the names for everything to be much shorter than they are.
+ [x] Move layout symbol to struct monitor from struct tag. This could save some memory considering we end up overwriting it most of the time anyway.
+ [x] In setup just make it so that most if not all global creations are used with an assert
+ [ ] Move all of the client.* file stuff to main.c same thing with window.h(?)
+ [x] Status in the bar.
+ [ ] Figure out a NULL selected_montior especially in reference to when monitors get disabled or disconnected.
+ [x] Move keychords and tag_config/pertag equivalent stuff into patches
+ [ ] Fix the keychord patch to use CLEAN_MOD_MASK()
+ [ ] Replace bemenu with wmenu as the default launcher. Once wmenu has a run script.
+ [x] If librewolf is floating and we create a download window then the download window stays below librewolf. Figure out why this is. See if this is dwm compliant, cause it doesn't seem like it.
+ [ ] Make text_draw() add ellipses to text too large to fit within the struct wlr_box. See dwm's drw_text() for more information on this.
+ [x] Fix the pixman errors if the status is very large.
+ [x] Fix the buttonbind thing so that it works cause right now we try to move the n cursor based on the title_width, which we calculate just completely incorrectly. Possibly just redo that entire function to make it super dumb like `buttonpress()` in dwm?
+ [ ] Update the cursor upon certain actions, like switching monitors and such. Create an update_cursor method which notify's clients about the cursor position being on them. So that they update the cursor image and stuff. This is already partially done in cursor_motion_handle() just extract that out into another function.
+ [ ] Improve the Makefile and use a config.mk file. See dwm's Makefile to get some ideas. Should probably stop using include files (If there are errors with these their caught in the compile phase and with much better messages) and instead just have the dirs incase we need them. Try and change the makefile to be posix make compatible and don't rely on gnu make stuff.
