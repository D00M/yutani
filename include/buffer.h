#ifndef BUFFER_H_
#define BUFFER_H_

#include <stddef.h>
#include <stdint.h>
#include <wlr/types/wlr_buffer.h>
#include <wlr/render/allocator.h>

struct buffer_allocator {
    struct wlr_allocator base;
};

struct buffer {
    struct wlr_buffer base;

    void *data;
    uint32_t format, bytes_per_block;
    size_t stride;
};

struct wlr_allocator *buffer_allocator_create(void);

// NOTE: Don't let this function signature fool you,
// this buffer type only supports DRM_FORMAT_ARGB8888
// cause we have no other way of dynamically
// supporting them without using another library and
// copying over some private wlroots code.
struct buffer *buffer_create(uint32_t width, uint32_t height, const struct wlr_drm_format *format);

#endif // BUFFER_H_
