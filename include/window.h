#ifndef WINDOW_H_
#define WINDOW_H_

enum window_type {
    WINDOW_BAR,
    WINDOW_TOPLEVEL,
    WINDOW_LAYERED,
};

// Essentially just an opaque type that is used
// to differentiate between different internal node types
// within yutani and apply safe type casting conventions using
// wl_container_of
struct window {
    enum window_type type;
};

#endif // WINDOW_H_
