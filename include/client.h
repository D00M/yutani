#ifndef CLIENT_H_
#define CLIENT_H_

#include <stdint.h>
#include <wayland-util.h>
#include <wlr/types/wlr_xdg_decoration_v1.h>
#include <wlr/util/box.h>
#include <wlr/types/wlr_compositor.h>
#include <wlr/types/wlr_keyboard.h>
#include "wlr-layer-shell-unstable-v1-protocol.h"
#include "window.h"

enum toplevel_type {
    TOPLEVEL_XDG,
    TOPLEVEL_X11,
};

struct toplevel {
    struct window base;
    enum toplevel_type type;
    struct monitor *monitor;
    // Some client backend specifc stuff
    union {
        struct {
            struct wlr_xdg_surface *surface;
            struct wlr_xdg_toplevel_decoration_v1 *decoration;
            struct {
                struct wl_listener commit;
                struct wl_listener maximize;
                struct wl_listener decoration_request;
                struct wl_listener decoration_destroy;
            } listeners;
        } xdg;
        struct {
            struct wlr_xwayland_surface *surface;
            struct {
                struct wl_listener associate;
                struct wl_listener dissociate;
                struct wl_listener activate;
                struct wl_listener configure;
                struct wl_listener hints;
            } listeners;
        } x11;
    };

    uint32_t tags, resize_serial, border_width;

    struct wlr_box geometry, bounds, previous_geometry;
    struct wlr_scene_tree *scene,     // The scene tree for the whole client
                          *surface;   // The tree for the client surface.
    struct wlr_scene_rect *border[4]; // The border rectangles for the client.

    bool floating, urgent, fullscreen;

    struct {
        struct wl_listener map;
        struct wl_listener unmap;
        struct wl_listener fullscreen;
        struct wl_listener destroy;
    } listeners;

    struct wl_list link, stack_link; // Are only used with xwayland and xdg client.
};

struct layered {
    struct window base;
    struct monitor *monitor;
    struct wlr_layer_surface_v1 *surface;
    struct wlr_scene_layer_surface_v1 *scene;
    struct wlr_scene_tree *popups;
    struct wlr_box geometry;

    struct {
        struct wl_listener destroy;
        struct wl_listener map;
        struct wl_listener unmap;
        struct wl_listener commit;
    } listeners;

    struct wl_list link;
};

bool client_from_surface(struct wlr_surface *surface, enum window_type *type, struct layered **layered, struct toplevel **toplevel);

bool toplevel_is_x11(struct toplevel *toplevel);
bool toplevel_is_xdg(struct toplevel *toplevel);
bool toplevel_is_stopped(struct toplevel *toplevel);
bool toplevel_wants_focus(struct toplevel *toplevel);
bool toplevel_wants_fullscreen(struct toplevel *toplevel);
bool toplevel_has_children(struct toplevel *toplevel);

// A shorthand description of an x11 thing where a client doesn't want the compositor to mess with this surface.
// Or might not want them to at least.
// See: https://tronche.com/gui/x/xlib/window/attributes/override-redirect.html
bool toplevel_is_unmanaged(struct toplevel *toplevel);

struct wlr_surface *toplevel_get_surface(struct toplevel *toplevel);
struct toplevel *toplevel_get_root(struct toplevel *toplevel);
bool toplevel_get_geometry(struct toplevel *toplevel, struct wlr_box *box);
bool toplevel_get_clip(struct toplevel *toplevel, struct wlr_box *box);
const char *toplevel_get_title(struct toplevel *toplevel);
const char *toplevel_get_appid(struct toplevel *toplevel);
struct toplevel *toplevel_get_parent(struct toplevel *toplevel);
bool toplevel_has_parent(struct toplevel *toplevel);

bool toplevel_restack(struct toplevel *toplevel);
bool toplevel_set_border(struct toplevel *toplevel, uint32_t color);
bool toplevel_set_tiled(struct toplevel *toplevel, uint32_t tiled);
bool toplevel_set_activated(struct toplevel *toplevel, bool activated);
bool toplevel_set_suspended(struct toplevel *toplevel, bool suspended);
bool toplevel_set_bounds(struct toplevel *toplevel, struct wlr_box *bounds);
int32_t toplevel_set_size(struct toplevel *toplevel, uint32_t width, uint32_t height);
bool toplevel_send_fullscreen(struct toplevel *toplevel, bool fullscreen);
bool toplevel_send_close(struct toplevel *toplevel);

#endif // CLIENT_H_
