#ifndef SWAPCHAIN_H_
#define SWAPCHAIN_H_

#include <stdbool.h>
#include <stdint.h>
#include <wayland-server-core.h>

#define SWAPCHAIN_SLOT_MAX 2

// Our own custom swapchain because
// 1. I want to
// and
// 2. The swapchain provided by wlroots only allows for static buffer sizes
// But we have the problem of that not being the case.
// And destroying a swapchain every time the monitor size changes (in the case a nested wayland session)
// doesn't seem like the most efficient thing in the world.

struct swapchain_slot {
    struct wlr_buffer *buffer;
    bool submitted;

    struct {
        struct wl_listener release;
    } listeners;
};

struct swapchain {
    struct wlr_allocator *allocator;
    struct swapchain_slot slots[SWAPCHAIN_SLOT_MAX];
    const struct wlr_drm_format *format;
};

bool swapchain_init(struct swapchain *chain, struct wlr_allocator *allocator, const struct wlr_drm_format *format);
bool swapchain_destroy(struct swapchain *chain);

struct wlr_buffer *swapchain_get(struct swapchain *chain, uint32_t width, uint32_t height);

bool swapchain_buffer_submitted(struct swapchain *chain, struct wlr_buffer *buffer);

#endif // SWAPCHAIN_H_
