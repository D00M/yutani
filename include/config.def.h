#ifndef CONFIG_H_
#define CONFIG_H_

#include <xkbcommon/xkbcommon-keysyms.h>
#include <wlr/types/wlr_output_layout.h>
#include <linux/input-event-codes.h>
#include <wlr/types/wlr_keyboard.h>
#include <libinput.h>
#include <stdint.h>

static const uint32_t gray1               = 0x222222ff;
static const uint32_t gray2               = 0x444444ff;
static const uint32_t gray3               = 0xbbbbbbff;
static const uint32_t gray4               = 0xeeeeeeff;
static const uint32_t cyan                = 0x005577ff;
static const uint32_t locked_color        = 0x191919ff;
static const uint32_t root_color          = 0x000000ff;
static const uint32_t fullscreen_bg_color = locked_color;
static const uint32_t colors[][3]         = {
    [SCHEME_NORMAL] = {
        [COLOR_FOREGROUND] = gray3,
        [COLOR_BACKGROUND] = gray1,
        [COLOR_BORDER]     = gray2,
    },
    [SCHEME_SELECTED] = {
        [COLOR_FOREGROUND] = gray4,
        [COLOR_BACKGROUND] = cyan,
        [COLOR_BORDER]     = cyan,
    },
    [SCHEME_URGENT] = {
        [COLOR_FOREGROUND] = gray1,
        [COLOR_BACKGROUND] = gray3,
        [COLOR_BORDER]     = cyan,
    },
};

static const char *fonts[] = {
    "monospace:size=13",
};


static const struct layout layouts[] = {
    { "[]=", tile },
    { "><>", NULL },
    { "[M]", monocle },
};

static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const struct app_rule app_rules[] = {};

static const uint32_t master_amount = 1; // The amount of clients to be put in the master stack.
static const float master_factor = 0.55; // The percentage of the screen the master stack will take up.
static const struct monitor_rule monitor_rules[] = {
    { NULL, 1, -1, -1, WL_OUTPUT_TRANSFORM_NORMAL },
};

#define MODIFIER WLR_MODIFIER_ALT

#define TAGKEYS(KEY, SHIFT_KEY, TAG) \
    {MODIFIER, (KEY),                                              view,        {.ui = (1 << (TAG))} }, \
    {MODIFIER|WLR_MODIFIER_CTRL,  (KEY),                           toggle_view, {.ui = (1 << (TAG))} }, \
    {MODIFIER|WLR_MODIFIER_SHIFT, (SHIFT_KEY),                     tag,         {.ui = (1 << (TAG))} }, \
    {MODIFIER|WLR_MODIFIER_SHIFT|WLR_MODIFIER_CTRL, (SHIFT_KEY),   toggle_tag,  {.ui = (1 << (TAG))} }

#define SHCMD(cmd) {.v = (const char *[]){"/bin/sh", "-c", cmd, NULL}}

static const char *menu[] = {"bemenu-run", NULL};
static const char *term[] = {"alacritty", NULL};

static const struct keybind keybinds[] = {
    {MODIFIER, XKB_KEY_p,                          spawn,             {.v = menu}},
    {MODIFIER|WLR_MODIFIER_SHIFT, XKB_KEY_Return,  spawn,             {.v = term}},
    {MODIFIER, XKB_KEY_b,                          toggle_bar,        {0}},
    {MODIFIER, XKB_KEY_j,                          focus_stack,       {.i = +1}},
    {MODIFIER, XKB_KEY_k,                          focus_stack,       {.i = -1}},
    {MODIFIER, XKB_KEY_i,                          master_set_amount, {.i = +1}},
    {MODIFIER, XKB_KEY_d,                          master_set_amount, {.i = -1}},
    {MODIFIER, XKB_KEY_h,                          master_set_factor, {.f = -0.05}},
    {MODIFIER, XKB_KEY_l,                          master_set_factor, {.f = +0.05}},
    {MODIFIER, XKB_KEY_Return,                     zoom,              {0}},
    {MODIFIER, XKB_KEY_Tab,                        view,              {0}},
    {MODIFIER|WLR_MODIFIER_SHIFT, XKB_KEY_C,       toplevel_kill,     {0}},
    {MODIFIER, XKB_KEY_t,                          set_layout,        {.v = &layouts[0]}},
    {MODIFIER, XKB_KEY_f,                          set_layout,        {.v = &layouts[1]}},
    {MODIFIER, XKB_KEY_m,                          set_layout,        {.v = &layouts[2]}},
    {MODIFIER, XKB_KEY_v,                          set_layout,        {.v = &layouts[3]}},
    {MODIFIER, XKB_KEY_space,                      set_layout,        {0}},
    {MODIFIER|WLR_MODIFIER_SHIFT, XKB_KEY_space,   toggle_floating,   {0}},
    {MODIFIER, XKB_KEY_e,                          toggle_fullscreen, {0}},
    {MODIFIER, XKB_KEY_0,                          view,              {.ui = ~0}},
    {MODIFIER|WLR_MODIFIER_SHIFT, XKB_KEY_0,       tag,               {.ui = ~0}},
    {MODIFIER, XKB_KEY_comma,                      monitor_focus,     {.ui = WLR_DIRECTION_LEFT}},
    {MODIFIER, XKB_KEY_period,                     monitor_focus,     {.ui = WLR_DIRECTION_RIGHT}},
    {MODIFIER|WLR_MODIFIER_SHIFT, XKB_KEY_less,    monitor_tag,       {.ui = WLR_DIRECTION_LEFT}},
    {MODIFIER|WLR_MODIFIER_SHIFT, XKB_KEY_greater, monitor_tag,       {.ui = WLR_DIRECTION_RIGHT}},
    {MODIFIER|WLR_MODIFIER_SHIFT, XKB_KEY_Q,       quit,              {0}},
	TAGKEYS(XKB_KEY_1, XKB_KEY_exclam,           0),
	TAGKEYS(XKB_KEY_2, XKB_KEY_at,               1),
	TAGKEYS(XKB_KEY_3, XKB_KEY_numbersign,       2),
	TAGKEYS(XKB_KEY_4, XKB_KEY_dollar,           3),
	TAGKEYS(XKB_KEY_5, XKB_KEY_percent,          4),
	TAGKEYS(XKB_KEY_6, XKB_KEY_asciicircum,      5),
	TAGKEYS(XKB_KEY_7, XKB_KEY_ampersand,        6),
	TAGKEYS(XKB_KEY_8, XKB_KEY_asterisk,         7),
	TAGKEYS(XKB_KEY_9, XKB_KEY_parenleft,        8),

    // For vts
#define CHVT(n) {WLR_MODIFIER_ALT|WLR_MODIFIER_CTRL, XKB_KEY_XF86Switch_VT_##n, chvt, {.ui = (n)}}
	CHVT(1), CHVT(2), CHVT(3), CHVT(4), CHVT(5), CHVT(6),
	CHVT(7), CHVT(8), CHVT(9), CHVT(10), CHVT(11), CHVT(12),
};

static const struct buttonbind buttonbinds[] = {
    {MODIFIER, BTN_LEFT,    CLICK_CLIENT, move_resize,     {.ui = CURSOR_MODE_MOVE}},
    {MODIFIER, BTN_MIDDLE,  CLICK_CLIENT, toggle_floating, {0}},
    {MODIFIER, BTN_RIGHT,   CLICK_CLIENT, move_resize,     {.ui = CURSOR_MODE_RESIZE}},
    {0,        BTN_LEFT,    CLICK_LAYOUT, set_layout,      {0}},
    {0,        BTN_RIGHT,   CLICK_LAYOUT, set_layout,      {.v = &layouts[length(layouts)-1]}},
    {0,        BTN_MIDDLE,  CLICK_TITLE,  zoom,            {0}},
    {0,        BTN_MIDDLE,  CLICK_STATUS, spawn,           {.v = term}},
    {0,        BTN_LEFT,    CLICK_TAG,    view,            {0}},
    {0,        BTN_RIGHT,   CLICK_TAG,    toggle_view,     {0}},
    {MODIFIER, BTN_LEFT,    CLICK_TAG,    tag,             {0}},
    {MODIFIER, BTN_RIGHT,   CLICK_TAG,    toggle_tag,      {0}},
};

static const uint32_t border_width = 1,
                      keyboard_repeat_rate  = 25,
                      keyboard_repeat_delay = 600;
static const bool     top_bar = true, // if not top then bottom
                      show_bar = true, // if false no bar on start
                      shortcuts_inhibits_pointer = true; // if true then keyboard shortcuts inhibitor protocol also inhibits pointer shortcuts.

// libinput configs
// see: https://wayland.freedesktop.org/libinput/doc/latest/configuration.html#
static const bool tap_to_click = true,
                  tap_and_drag = true,
                  drag_lock = true,
                  natural_scrolling = false,
                  disable_while_typing = true,
                  left_handed = false,
                  middle_button_emulation = false;
static const uint32_t send_events_mode = LIBINPUT_CONFIG_SEND_EVENTS_ENABLED;
static const enum libinput_config_scroll_method  scroll_method = LIBINPUT_CONFIG_SCROLL_2FG;
static const enum libinput_config_click_method   click_method = LIBINPUT_CONFIG_CLICK_METHOD_BUTTON_AREAS;
static const enum libinput_config_tap_button_map button_map = LIBINPUT_CONFIG_TAP_MAP_LRM;
static const enum libinput_config_accel_profile  accel_profile = LIBINPUT_CONFIG_ACCEL_PROFILE_ADAPTIVE;
static const double accel_speed = 0.0;

#endif // CONFIG_H_
