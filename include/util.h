#ifndef UTIL_H_
#define UTIL_H_

#include <stddef.h>
#include <stdint.h>

#define panic(fmt, ...) _panic(__LINE__, __FILE__, fmt, ##__VA_ARGS__)
#define panic_errno(fmt, ...) _panic(__LINE__, __FILE__, fmt " errno: %s", ##__VA_ARGS__, strerror(errno))
#define assert(expr) if (!(expr)) panic("Assert failed :: " #expr);
#define listen(signal, listener, notify_func) (wl_signal_add((signal), (((listener)->notify = (notify_func), (listener)))))
#define length(array) (sizeof((array)) / sizeof((array)[0]))

// This is a lot but we're essentially just isolating rgba value in our color
// then we're duplicating the bits so they fill the whole 16 bit value in the
// pixman color.
#define PIXMAN_COLOR(hex) (pixman_color_t){ \
    .red    = ((0x0000 | ((hex >> 24) & 0xff)) | (((hex >> 24) & 0xff) << 8)), \
    .green  = ((0x0000 | ((hex >> 16) & 0xff)) | (((hex >> 16) & 0xff) << 8)), \
    .blue   = ((0x0000 | ((hex >>  8) & 0xff)) | (((hex >>  8) & 0xff) << 8)), \
    .alpha  = ((0x0000 | (hex         & 0xff)) | ((hex         & 0xff) << 8)), \
}
#define GLSL_COLOR(hex) (float[]){ \
    ((hex >> 24) & 0xff) / 255.0f, \
    ((hex >> 16) & 0xff) / 255.0f, \
    ((hex >> 8)  & 0xff) / 255.0f, \
    ( hex        & 0xff) / 255.0f, \
}

void _panic(uint32_t line, const char *file, const char *fmt, ...);
void *ecalloc(size_t blocks, size_t size);
void *ezalloc(size_t size);

#endif // UTIL_H_
