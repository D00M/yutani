#include <stdint.h>
#include <wayland-server-core.h>
#include <wayland-util.h>
#include <wlr/types/wlr_compositor.h>
#include <wlr/types/wlr_layer_shell_v1.h>
#include <wlr/types/wlr_xdg_shell.h>
#include <wlr/types/wlr_scene.h>
#include <wlr/util/edges.h>
#include <wlr/util/log.h>
#include <wlr/xwayland.h>
#include <wayland-server.h>
#include <sys/wait.h>
#include <xcb/xproto.h>
#include "client.h"
#include "window.h"
#include "util.h"
#include "xdg-shell-protocol.h"

// Don't really like this function cause this can get ugly quick if new client types pop up.
bool client_from_surface(struct wlr_surface *surface, enum window_type *type, struct layered **layered, struct toplevel **toplevel) {
    if (!surface) return false;

    struct toplevel *client_toplevel = NULL;
    struct layered *client_layered = NULL;
    enum window_type window_type;

    // Get the root of a subsurface if there is one. If this surface happens to be one.
    surface = wlr_surface_get_root_surface(surface);
    if (!surface) return false;

    struct wlr_xwayland_surface *xwayland_surface;
    if ((xwayland_surface = wlr_xwayland_surface_try_from_wlr_surface(surface))) {
        client_toplevel = xwayland_surface->data;
        window_type = WINDOW_TOPLEVEL;
    }

    struct wlr_layer_surface_v1 *layer_surface;
    if ((layer_surface = wlr_layer_surface_v1_try_from_wlr_surface(surface))) {
        client_layered = layer_surface->data;
        window_type = WINDOW_LAYERED;
        goto done;
    }

    struct wlr_xdg_surface *xdg_surface;
    if (!(xdg_surface = wlr_xdg_surface_try_from_wlr_surface(surface))) {
        wlr_log(WLR_ERROR, "Found an unknown surface role when searching for root client");
        goto done;
    }

    while (xdg_surface) {
        switch (xdg_surface->role) {
            case WLR_XDG_SURFACE_ROLE_TOPLEVEL:
                client_toplevel = xdg_surface->data;
                window_type = WINDOW_TOPLEVEL;
                goto done;
                break;
            case WLR_XDG_SURFACE_ROLE_POPUP:
                if (!xdg_surface->popup || !xdg_surface->popup->parent) goto done;

                struct wlr_xdg_surface *tmp = wlr_xdg_surface_try_from_wlr_surface(xdg_surface->popup->parent);
                if (!tmp) return client_from_surface(xdg_surface->popup->parent, type, layered, toplevel);

                xdg_surface = tmp;
                break;
            case WLR_XDG_SURFACE_ROLE_NONE:
                wlr_log(WLR_ERROR, "Got an xdg_surface with no role");
                goto done;
                break;
        }
    }

done:
    if (layered) *layered = client_layered;
    if (toplevel) *toplevel = client_toplevel;
    if (type && (client_layered || client_toplevel)) *type = window_type;

    return (client_toplevel || client_layered);
}

bool toplevel_is_x11(struct toplevel *toplevel) {
    return toplevel && toplevel->type == TOPLEVEL_X11;
}

bool toplevel_is_xdg(struct toplevel *toplevel) {
    return toplevel && toplevel->type == TOPLEVEL_XDG;
}

bool toplevel_is_stopped(struct toplevel *toplevel) {
    if (!toplevel || toplevel_is_x11(toplevel)) return false;

	int pid;
	siginfo_t in = {0};
	wl_client_get_credentials(toplevel->xdg.surface->client->client, &pid, NULL, NULL);
	if (waitid(P_PID, pid, &in, WNOHANG|WCONTINUED|WSTOPPED|WNOWAIT) < 0) {
		/* This process is not our child process, while it is very unlucky that
		 * it is stopped, in order to do not skip frames assume that it is. */
		if (errno == ECHILD)
			return true;
	} else if (in.si_pid) {
		if (in.si_code == CLD_STOPPED || in.si_code == CLD_TRAPPED)
			return true;
		if (in.si_code == CLD_CONTINUED)
			return false;
	}

    return false;
}

bool toplevel_wants_focus(struct toplevel *toplevel) {
    return toplevel &&
        toplevel_is_unmanaged(toplevel) &&
        wlr_xwayland_or_surface_wants_focus(toplevel->x11.surface) &&
        wlr_xwayland_icccm_input_model(toplevel->x11.surface) != WLR_ICCCM_INPUT_MODEL_NONE;
}

bool toplevel_wants_fullscreen(struct toplevel *toplevel) {
    return toplevel && (toplevel_is_x11(toplevel)
        ? toplevel->x11.surface->fullscreen
        : toplevel->xdg.surface->toplevel->requested.fullscreen);
}

bool toplevel_has_children(struct toplevel *toplevel) {
    if (!toplevel) return false;

    if (toplevel_is_x11(toplevel)) return !wl_list_empty(&toplevel->x11.surface->children);

    return (toplevel->xdg.surface->link.next != toplevel->xdg.surface->link.prev);
}

bool toplevel_is_unmanaged(struct toplevel *toplevel) {
    return toplevel && toplevel_is_x11(toplevel) && toplevel->x11.surface->override_redirect;
}

struct wlr_surface *toplevel_get_surface(struct toplevel *toplevel) {
    if (!toplevel) return NULL;
    if (toplevel_is_x11(toplevel)) {
        return toplevel->x11.surface->surface;
    }
    else {
        return toplevel->xdg.surface->surface;
    }
}

bool toplevel_get_geometry(struct toplevel *toplevel, struct wlr_box *box) {
    if (!toplevel || !box) return false;

    if (toplevel_is_x11(toplevel)) {
        box->x = toplevel->x11.surface->x;
        box->y = toplevel->x11.surface->y;
        box->width = toplevel->x11.surface->width;
        box->height = toplevel->x11.surface->height;
        return true;
    }

    wlr_xdg_surface_get_geometry(toplevel->xdg.surface, box);
    return true;
}

bool toplevel_get_clip(struct toplevel *toplevel, struct wlr_box *box) {
    if (!toplevel || !box) return false;

    box->x = 0;
    box->y = 0;
    box->width  = toplevel->geometry.width - toplevel->border_width;
    box->height = toplevel->geometry.height - toplevel->border_width;

    if (toplevel_is_x11(toplevel)) return true;

    struct wlr_box geom;
    wlr_xdg_surface_get_geometry(toplevel->xdg.surface, &geom);
    box->x = geom.x;
    box->y = geom.y;

    return true;
}

const char *toplevel_get_title(struct toplevel *toplevel) {
    if (toplevel_is_x11(toplevel)) {
        return toplevel->x11.surface->title;
    }
    else {
        return toplevel->xdg.surface->toplevel->title;
    }
}

const char *toplevel_get_appid(struct toplevel *toplevel) {
    if (toplevel_is_x11(toplevel)) {
        return toplevel->x11.surface->class;
    }
    else {
        return toplevel->xdg.surface->toplevel->app_id;
    }
}

struct toplevel *toplevel_get_parent(struct toplevel *toplevel) {
    if (!toplevel || !toplevel_has_parent(toplevel)) return NULL;

    struct toplevel *out;
    struct wlr_surface *parent = toplevel_is_x11(toplevel) ?
        toplevel->x11.surface->parent->surface :
        toplevel->xdg.surface->toplevel->parent->base->surface;

    client_from_surface(parent, NULL, NULL, &out);

    return out;
}

bool toplevel_has_parent(struct toplevel *toplevel) {
    if (!toplevel) return false;

    switch (toplevel->type) {
        case TOPLEVEL_XDG:
            return (bool)toplevel->xdg.surface->toplevel->parent;
        case TOPLEVEL_X11:
            return (bool)toplevel->x11.surface->parent;
    }

    return false;
}

bool toplevel_restack(struct toplevel *toplevel) {
    if (!toplevel) return false;
    if (toplevel_is_xdg(toplevel)) return true;

    wlr_xwayland_surface_restack(toplevel->x11.surface, NULL, XCB_STACK_MODE_ABOVE);

    return true;
}

bool toplevel_set_border(struct toplevel *toplevel, uint32_t color) {
    if (!toplevel) return false;

    for (int i = 0; i < 4; i++) wlr_scene_rect_set_color(toplevel->border[i], GLSL_COLOR(color));

    return true;
}

bool toplevel_set_tiled(struct toplevel *toplevel, uint32_t edges) {
    if (!toplevel) return false;
    if (!toplevel_is_xdg(toplevel)) return true;

	if (wl_resource_get_version(toplevel->xdg.surface->toplevel->resource) >= XDG_TOPLEVEL_STATE_TILED_RIGHT_SINCE_VERSION) {
		wlr_xdg_toplevel_set_tiled(toplevel->xdg.surface->toplevel, edges);
	} else {
		wlr_xdg_toplevel_set_maximized(toplevel->xdg.surface->toplevel, edges != WLR_EDGE_NONE);
	}

    return true;
}

bool toplevel_set_activated(struct toplevel *toplevel, bool activated) {
    if (!toplevel) return false;

    if (toplevel_is_x11(toplevel)) {
        wlr_xwayland_surface_activate(toplevel->x11.surface, activated);
        return true;
    }

    wlr_xdg_toplevel_set_activated(toplevel->xdg.surface->toplevel, activated);
    return true;
}

bool toplevel_set_suspended(struct toplevel *toplevel, bool suspended) {
    if (!toplevel) return false;

    if (toplevel_is_x11(toplevel)) {
        wlr_xwayland_surface_set_withdrawn(toplevel->x11.surface, suspended);
        return true;
    }

    wlr_xdg_toplevel_set_suspended(toplevel->xdg.surface->toplevel, suspended);
    return true;
}

bool toplevel_set_bounds(struct toplevel *toplevel, struct wlr_box *bounds) {
    if (!toplevel || toplevel_is_x11(toplevel)) return false;

    if (wl_resource_get_version(toplevel->xdg.surface->toplevel->resource) >= XDG_TOPLEVEL_CONFIGURE_BOUNDS &&
            bounds->width >= 0 && bounds->height >= 0 &&
            (toplevel->bounds.width != bounds->width || toplevel->bounds.height != bounds->height)) {
        toplevel->bounds.width = bounds->width;
        toplevel->bounds.height = bounds->height;
        return wlr_xdg_toplevel_set_bounds(toplevel->xdg.surface->toplevel, toplevel->bounds.width, toplevel->bounds.height);
    }

    return false;
}

int32_t toplevel_set_size(struct toplevel *toplevel, uint32_t width, uint32_t height) {
    if (!toplevel) return 0;

    if (toplevel_is_x11(toplevel)) {
        wlr_xwayland_surface_configure(toplevel->x11.surface, toplevel->geometry.x, toplevel->geometry.y, width, height);
        return 0;
    }

    if (width == toplevel->xdg.surface->current.geometry.width && height == toplevel->xdg.surface->current.geometry.height) {
        return 0;
    }

    return wlr_xdg_toplevel_set_size(toplevel->xdg.surface->toplevel, width, height);
}

bool toplevel_send_fullscreen(struct toplevel *toplevel, bool fullscreen) {
    if (!toplevel) return false;

    if (toplevel_is_x11(toplevel)) {
        wlr_xwayland_surface_set_fullscreen(toplevel->x11.surface, fullscreen);
        return true;
    }

    wlr_xdg_toplevel_set_fullscreen(toplevel->xdg.surface->toplevel, fullscreen);

    return true;
}

bool toplevel_send_close(struct toplevel *toplevel) {
    if (!toplevel) return false;

    if (toplevel_is_x11(toplevel)) {
        wlr_xwayland_surface_close(toplevel->x11.surface);
        return true;
    }

    wlr_xdg_toplevel_send_close(toplevel->xdg.surface->toplevel);

    return true;
}
