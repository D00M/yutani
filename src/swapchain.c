#include <string.h>
#include <wayland-server-core.h>
#include <wayland-util.h>
#include <wlr/types/wlr_buffer.h>
#include <wlr/render/allocator.h>
#include "swapchain.h"
#include "util.h"

static void buffer_release(struct wl_listener *listener, void *data);

void buffer_release(struct wl_listener *listener, void *data) {
    struct swapchain_slot *slot = wl_container_of(listener, slot, listeners.release);
    slot->submitted = false;
}

bool swapchain_init(struct swapchain *chain, struct wlr_allocator *allocator, const struct wlr_drm_format *format) {
    if (!chain || !allocator || !format) return false;

    memset(chain, 0, sizeof(*chain));

    chain->allocator = allocator;
    chain->format = format;

    return true;
}

bool swapchain_destroy(struct swapchain *chain) {
    if (!chain) return false;

    for (int i = 0; i < SWAPCHAIN_SLOT_MAX; i++) {
        struct swapchain_slot *slot = &chain->slots[i];
        if (slot->buffer) {
            wl_list_remove(&slot->listeners.release.link);
            wlr_buffer_drop(slot->buffer);
        }
        slot->buffer = NULL;
        slot->submitted = false;
    }

    return true;
}

struct wlr_buffer *swapchain_get(struct swapchain *chain, uint32_t width, uint32_t height) {
    if (!chain || !width || !height) return NULL;

    struct swapchain_slot *free_slot = NULL;
    for (int i = 0; i < SWAPCHAIN_SLOT_MAX; i++) {
        struct swapchain_slot *slot = &chain->slots[i];
        if (slot->submitted) continue;
        if (slot->buffer && slot->buffer->width == width && slot->buffer->height == height) return slot->buffer;
        if (!slot->buffer || slot->buffer->width != width || slot->buffer->height != height) free_slot = slot;
    }

    if (!free_slot) return NULL;

    if (free_slot->buffer) {
        wl_list_remove(&free_slot->listeners.release.link);
        wlr_buffer_drop(free_slot->buffer);
    }

    free_slot->buffer = wlr_allocator_create_buffer(chain->allocator, width, height, chain->format);
    if (!free_slot->buffer) return NULL;

    listen(&free_slot->buffer->events.release, &free_slot->listeners.release, buffer_release);

    return free_slot->buffer;
}

bool swapchain_buffer_submitted(struct swapchain *chain, struct wlr_buffer *buffer) {
    if (!chain || !buffer) return false;

    for (int i = 0; i < SWAPCHAIN_SLOT_MAX; i++) {
        struct swapchain_slot *slot = &chain->slots[i];
        if (slot->buffer != buffer) continue;

        slot->submitted = true;

        return true;
    }

    return false;
}
