#include <assert.h>
#include <libinput.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <drm_fourcc.h>
#include <pixman.h>
#include <fcntl.h>
#include <fcft/fcft.h>
#include <fcft/stride.h>
#include <wayland-server-core.h>
#include <wayland-server-protocol.h>
#include <wayland-util.h>
#include <wlr/backend.h>
#include <wlr/backend/session.h>
#include <wlr/render/wlr_renderer.h>
#include <wlr/render/allocator.h>
#include <wlr/types/wlr_relative_pointer_v1.h>
#include <wlr/types/wlr_pointer_constraints_v1.h>
#include <wlr/types/wlr_compositor.h>
#include <wlr/types/wlr_subcompositor.h>
#include <wlr/types/wlr_data_device.h>
#include <wlr/types/wlr_xdg_shell.h>
#include <wlr/types/wlr_export_dmabuf_v1.h>
#include <wlr/types/wlr_screencopy_v1.h>
#include <wlr/types/wlr_data_control_v1.h>
#include <wlr/types/wlr_primary_selection_v1.h>
#include <wlr/types/wlr_viewporter.h>
#include <wlr/types/wlr_single_pixel_buffer_v1.h>
#include <wlr/types/wlr_fractional_scale_v1.h>
#include <wlr/types/wlr_xdg_activation_v1.h>
#include <wlr/types/wlr_layer_shell_v1.h>
#include <wlr/types/wlr_idle_notify_v1.h>
#include <wlr/types/wlr_keyboard_shortcuts_inhibit_v1.h>
#include <wlr/types/wlr_idle_inhibit_v1.h>
#include <wlr/types/wlr_gamma_control_v1.h>
#include <wlr/types/wlr_session_lock_v1.h>
#include <wlr/types/wlr_server_decoration.h>
#include <wlr/types/wlr_xdg_decoration_v1.h>
#include <wlr/types/wlr_xdg_output_v1.h>
#include <wlr/types/wlr_output_layout.h>
#include <wlr/types/wlr_output_management_v1.h>
#include <wlr/types/wlr_presentation_time.h>
#include <wlr/types/wlr_cursor_shape_v1.h>
#include <wlr/types/wlr_xcursor_manager.h>
#include <wlr/types/wlr_virtual_keyboard_v1.h>
#include <wlr/types/wlr_virtual_pointer_v1.h>
#include <wlr/types/wlr_primary_selection.h>
#include <wlr/types/wlr_keyboard_group.h>
#include <wlr/backend/libinput.h>
#include <wlr/types/wlr_region.h>
#include <wlr/types/wlr_cursor.h>
#include <wlr/types/wlr_scene.h>
#include <wlr/types/wlr_seat.h>
#include <wlr/types/wlr_drm.h>
#include <wlr/util/region.h>
#include <wlr/util/box.h>
#include <wlr/util/log.h>
#include <wlr/xwayland.h>
#include <wlr/xcursor.h>
#include <wlr/xcursor.h>
#include <wayland-server.h>
#include <xkbcommon/xkbcommon.h>
#include <xcb/xcb_icccm.h>
#include <xcb/xproto.h>
#include <xcb/xcb.h>
#include "client.h"
#include "swapchain.h"
#include "utf8.h"
#include "util.h"
#include "window.h"
#include "buffer.h"

#define END(array) ((array) + length(array))
#define MIN(x, y) ((x) < (y) ? (x) : (y))
#define MAX(x, y) ((x) > (y) ? (x) : (y))
#define CLEAN_MOD_MASK(mods) (mods & ~WLR_MODIFIER_CAPS)
#define TAGMASK ((1 << length(tags)) - 1)
#define CLIENT_VISIBLE_ON(client, mon) ((mon) && (client)->monitor == (mon) && (client)->tags & (mon)->tagset[(mon)->sel_tags])
#define STRING_EQUAL(str1, str2) (strcmp(str1, str2) == 0)
#define STRINGN_EQUAL(str1, str2, n) (strncmp(str1, str2, n) == 0)
#define DEFAULT_STATUS ("yutani " VERSION)
#define WLR_LAYER_SHELL_LAYERS 4
#define LAYOUT_SYMBOL_MAX 16

union arg;

typedef void (*layout_func)(struct monitor *monitor);
typedef void (*bind_func)(const union arg *arg);

enum click_location {
    CLICK_NONE, // If none of the below, ie. the background of the environment
    CLICK_CLIENT,
    CLICK_TAG,
    CLICK_LAYOUT,
    CLICK_TITLE,
    CLICK_STATUS,
};

enum color_scheme {
    SCHEME_NORMAL   = 0,
    SCHEME_SELECTED = 1,
    SCHEME_URGENT   = 2,
};

enum scheme_color {
    COLOR_FOREGROUND = 0,
    COLOR_BACKGROUND = 1,
    COLOR_BORDER     = 2,
};

enum net_wm_atom {
    NetWMWindowTypeDialog,
    NetWMWindowTypeSplash,
    NetWMWindowTypeToolbar,
    NetWMWindowTypeUtility,
    NetLast,
};

enum cursor_mode {
    CURSOR_MODE_RELEASED = 0,
    CURSOR_MODE_PRESSED,
    CURSOR_MODE_MOVE,
    CURSOR_MODE_RESIZE,
};

enum layer {
    BACKGROUND = 0,
    BOTTOM     = 1,
    TILED      = 2,
    FLOATING   = 3,
    FULLSCREEN = 4,
    TOP        = 5,
    OVERLAY    = 6,
    LOCK       = 7,
    NUM_LAYERS,
};

struct layout {
    const char *symbol; // the displayed symbol / name of the layout
    layout_func arrange;
};

// To use an app rule you must to specify a title or an appid (or both), and a view name. The monitor name is optional.
// For example:
// { "firefox", NULL, "eDP-1", "1" },
struct app_rule {
    const char *title, *appid, *monitor;
    bool floating, fullscreen;
    uint32_t tags;
};

// To use a monitor rule you must specify a monitor name then the master amount,
// along with a master multiplier, a scale, and a transform. Optionally you may
// specify an x and y. Note that a monitor name may not be specified, this rule
// will act as a default value, the first NULL monitor name encountered will be used
// as the default.
// If x and y aren't specified then the position of the monitor is determined by the
// wlr_output_layout_add_auto() function.
// The master_amount is the amount of clients held in the master stack, and the multiplier
// is the amount of space that the master stack takes up.
// For example:
// { "eDP-1", 1, -1, -1, WL_OUTPUT_TRANSFORM_NORMAL },
struct monitor_rule {
    const char *monitor;
    float scale;
    int32_t x, y;
    enum wl_output_transform transform;
};

union arg {
    uint32_t ui;
    int32_t i;
    float f;
    const char *str;
    const void *v;
};

struct keybind {
    uint32_t mods;
    xkb_keysym_t keysym;
    bind_func func;
    const union arg arg;
};

struct buttonbind {
    uint32_t mods, button;
    enum click_location click;
    bind_func func;
    const union arg arg;
};

struct pointer {
    struct wlr_pointer *pointer;

    struct {
        struct wl_listener destroy;
    } listeners;

    struct wl_list link;
};

struct pointer_constraint {
    struct wlr_pointer_constraint_v1 *contraint;

    struct {
        struct wl_listener destroy;
    } listeners;
};

struct keyboard {
    struct keyboard_group *group;
    struct wlr_keyboard *keyboard;

    struct {
        struct wl_listener destroy;
    } listeners;

    struct wl_list link;
};

struct keyboard_group {
    struct wlr_keyboard_group *group;
    struct wl_list keyboards; // struct keyboard

    const xkb_keysym_t *keysyms;
    uint32_t mods, keysyms_len;
    struct wl_event_source *key_repeat;

    struct {
        struct wl_listener key;
        struct wl_listener modifiers;
    } listeners;
};

struct monitor {
    struct wlr_output *output;
    struct wlr_scene_output *output_scene;
    struct wlr_box geometry, toplevel_space;
    struct wlr_session_lock_surface_v1 *lock_surface; // can be NULL
    struct wlr_scene_rect *fullscreen_background;
    bool gamma_changed;

    struct toplevel *sel; // can be NULL
    struct wl_list toplevels,          // struct toplevel.link; the list of mapped clients
                   stack,              // struct toplevel.stack; the focust list of mapped clients
                   layers[WLR_LAYER_SHELL_LAYERS];

    uint32_t tagset[2],
             sel_tags,
             master_amount;
    float master_factor;
    const struct layout *layouts[2];
    uint32_t layout;
    char lt_symbol[LAYOUT_SYMBOL_MAX];

    struct {
        struct window base;
        struct wlr_scene_buffer *scene;
        struct wlr_box geometry;
        struct swapchain swapchain;
    } bar;

    struct {
        struct wl_listener frame;
        struct wl_listener request_state;
        struct wl_listener destroy;
        struct wl_listener lock_surface_destroy;
        struct wl_listener frame_done;
    } listeners;

    struct wl_list link;
};

static void box_apply_bounds(struct wlr_box *box, struct wlr_box *bound);
static bool buttonbind(uint32_t button);
static void chvt(const union arg *arg);
static void cleanup(void);
static void client_notify_enter(struct wlr_surface *surface, struct wlr_keyboard *keyboard);
static void cursor_axis(struct wl_listener *listener, void *data);
static void cursor_button(struct wl_listener *listener, void *data);
static void cursor_frame(struct wl_listener *listener, void *data);
static void cursor_motion(struct wl_listener *listener, void *data);
static void cursor_motion_absolute(struct wl_listener *listener, void *data);
static void cursor_motion_handle(struct wlr_input_device *device, uint32_t time_msec, double dx, double dy, double dxa, double dya);
static void cursor_shape_set_request(struct wl_listener *listener, void *data);
static void decoration_destroy(struct wl_listener *listener, void *data);
static void decoration_request(struct wl_listener *listener, void *data);
static void drag_icon_destroy(struct wl_listener *listener, void *data);
static void draw_bar(struct wl_listener *listener, void *data);
static void focus_stack(const union arg *arg);
static void gamma_changed(struct wl_listener *listener, void *data);
static void idle_inhibitor_destroy(struct wl_listener *listener, void *data);
static bool keybind(struct keyboard_group *group, const xkb_keysym_t keysym, uint32_t mods);
static void keyboard_destroy(struct wl_listener *listener, void *data);
static void keyboard_init(struct keyboard_group *group, struct wlr_keyboard *keyboard);
static void keyboard_key(struct wl_listener *listener, void *data);
static void keyboard_modifiers(struct wl_listener *listener, void *data);
static int32_t keyboard_repeat(void *data);
static void layered_commit(struct wl_listener *listener, void *data);
static void layered_destroy(struct wl_listener *listener, void *data);
static void layered_map(struct wl_listener *listener, void *data);
static struct layered *layered_try_from_window(struct window *window);
static void layered_unmap(struct wl_listener *listener, void *data);
static void lock_destroy();
static void lock_manager_destroy(struct wl_listener *listener, void *data);
static void lock_surface_destroy(struct wl_listener *listener, void *data);
static void master_set_amount(const union arg *arg);
static void master_set_factor(const union arg *arg);
static void monitor_arrange(struct monitor *monitor);
static void monitor_cleanup(struct monitor *monitor);
static void monitor_destroy(struct wl_listener *listener, void *data);
static struct monitor *monitor_enabled_in_direction(struct monitor *monitor, enum wlr_direction direction);
static void monitor_focus(const union arg *arg);
static void monitor_frame(struct wl_listener *listener, void *data);
static struct monitor *monitor_from_xy(uint32_t x, uint32_t y);
static struct toplevel *monitor_get_top(struct monitor *monitor);
static void monitor_layers_arrange(struct monitor *monitor);
static void monitor_layer_arrange(struct monitor *monitor, enum zwlr_layer_shell_v1_layer layer, struct wlr_box *usable, bool exclusive);
static void monitor_request_state(struct wl_listener *listener, void *data);
static void monitor_tag(const union arg *arg);
static void monitor_update_config(struct monitor *monitor, struct wlr_output_configuration_v1 *config);
static void monitor_update_geometry(struct monitor *monitor);
static void monitor_update_selected(void);
static void monocle(struct monitor *monitor);
static void move_resize(const union arg *arg);
static void new_decoration(struct wl_listener *listener, void *data);
static void new_idle_inhibitor(struct wl_listener *listener, void *data);
static void new_input(struct wl_listener *listener, void *data);
static void new_layer_client(struct wl_listener *listener, void *data);
static void new_lock(struct wl_listener *listener, void *data);
static void new_lock_surface(struct wl_listener *listener, void *data);
static void new_output(struct wl_listener *listener, void *data);
static void new_pointer_constraint(struct wl_listener *listener, void *data);
static void new_shortcut_inhibitor(struct wl_listener *listener, void *data);
static void new_virtual_keyboard(struct wl_listener *listener, void *data);
static void new_virtual_pointer(struct wl_listener *listener, void *data);
static void new_xdg_client(struct wl_listener *listener, void *data);
static void new_xwayland_client(struct wl_listener *listener, void *data);
static void output_layout_change(struct wl_listener *listener, void *data);
static void output_manager_apply(struct wl_listener *listener, void *data);
static void output_manager_change(struct wlr_output_configuration_v1 *config, bool test);
static void output_manager_test(struct wl_listener *listener, void *data);
static void pointer_constrain(struct pointer_constraint *constraint);
static void pointer_constraint_destroy(struct wl_listener *listener, void *data);
static void pointer_destroy(struct wl_listener *listener, void *data);
static void pointer_init(struct wlr_pointer *wlr_pointer);
static void quit(const union arg *arg);
static void run(void);
static void seat_set_cursor(struct wl_listener *listener, void *data);
static void seat_set_primary_selection(struct wl_listener *listener, void *data);
static void seat_set_request_start_drag(struct wl_listener *listener, void *data);
static void seat_set_selection(struct wl_listener *listener, void *data);
static void seat_set_start_drag(struct wl_listener *listener, void *data);
static void seat_update_capabilities(void);
static void session_lock_destroy(struct wl_listener *listener, void *data);
static void setup(void);
static void set_layout(const union arg *arg);
static void shortcut_inhibitor_destroy(struct wl_listener *listener, void *data);
static void signal_handle(int signal);
static void spawn(const union arg *arg);
static int32_t stdin_in(int32_t fd, uint32_t mask, void *data);
static void tag(const union arg *arg);
static uint32_t text_draw(struct fcft_font *font, pixman_image_t *image, const char *text, const pixman_color_t *text_color, const struct wlr_box *box);
static uint32_t text_width(struct fcft_font *font, const char *text);
static void tile(struct monitor *monitor);
static void toggle_bar(const union arg *arg);
static void toggle_floating(const union arg *arg);
static void toggle_fullscreen(const union arg *arg);
static void toggle_tag(const union arg *arg);
static void toggle_view(const union arg *arg);
static void toplevel_activate(struct wl_listener *listener, void *data);
static void toplevel_apply_rules(struct toplevel *toplevel);
static void toplevel_associate(struct wl_listener *listener, void *data);
static void toplevel_commit(struct wl_listener *listener, void *data);
static void toplevel_configure(struct wl_listener *listener, void *data);
static void toplevel_destroy(struct wl_listener *listener, void *data);
static void toplevel_dissociate(struct wl_listener *listener, void *data);
static void toplevel_focus(struct toplevel *toplevel, bool raise);
static struct toplevel *toplevel_from_xy(uint32_t x, uint32_t y, double *nx, double *ny);
static void toplevel_fullscreen(struct wl_listener *listener, void *data);
static void toplevel_hints(struct wl_listener *listener, void *data);
static bool toplevel_is_rendered(struct toplevel *toplevel, struct monitor *monitor);
static void toplevel_kill(const union arg *arg);
static void toplevel_map(struct wl_listener *listener, void *data);
static void toplevel_maximize(struct wl_listener *listener, void *data);
static void toplevel_resize(struct toplevel *toplevel, struct wlr_box *bound, struct wlr_box *geometry);
static void toplevel_set_floating(struct toplevel *toplevel, bool floating);
static void toplevel_set_fullscreen(struct toplevel *toplevel, bool fullscreen);
static void toplevel_set_monitor(struct toplevel *toplevel, struct monitor *monitor, uint32_t tags);
static struct toplevel *toplevel_try_from_window(struct window *window);
static void toplevel_unfocus(struct toplevel *toplevel);
static void toplevel_unmap(struct wl_listener *listener, void *data);
static bool toplevel_wants_floating(struct toplevel *toplevel);
static void unlock(struct wl_listener *listener, void *data);
static void view(const union arg *arg);
static struct window *window_from_xy(uint32_t x, uint32_t y, double *nx, double *ny, struct wlr_surface **surface);
static void xdg_activation_activate(struct wl_listener *listener, void *data);
static xcb_atom_t xwayland_get_atom(xcb_connection_t *connection, const char *atom);
static void xwayland_ready(struct wl_listener *listener, void *data);
static void zoom(const union arg *arg);

static struct wl_display *display;
static struct wlr_backend *backend;
static struct wlr_session *session = NULL;
static struct wlr_renderer *renderer;
static struct wlr_allocator *allocator;
static struct wlr_output_layout *output_layout;
static struct wlr_cursor *cursor;
static struct wlr_xcursor_manager *xcursor;
static struct wlr_seat *seat;
static struct wlr_xwayland *xwayland;

static struct wlr_scene *scene;
static struct wlr_box scene_geometry;
static struct wlr_scene_rect *root_background;
static struct wlr_scene_tree *layers[NUM_LAYERS],
                             *drag_icon;

static struct fcft_font *font;

static struct keyboard_group keyboards, virtual_keyboards;
static struct wl_list pointers; // struct pointer

static struct monitor *sel_mon; // can be NULL
static struct wl_list monitors; // struct monitor

static enum cursor_mode cursor_mode;

static struct {
    struct wlr_session_lock_v1 *lock;
    struct wlr_scene_tree *surfaces;
    struct wlr_scene_rect *background;
} session_lock;
static bool locked = false;

static struct {
    struct wlr_cursor_shape_manager_v1 *cursor_shape;
    struct wlr_idle_notifier_v1 *idle_notifier;
    struct wlr_idle_inhibit_manager_v1 *idle_inhibit;
    struct wlr_keyboard_shortcuts_inhibit_manager_v1 *shortcuts_inhibitor;
    struct wlr_gamma_control_manager_v1 *gamma_control;
    struct wlr_compositor *compositor;
    struct wlr_output_manager_v1 *output_manager;
    struct wlr_relative_pointer_manager_v1 *relative_manager;
    struct wlr_pointer_constraints_v1 *pointer_constraints;
} globals;

static struct {
    struct wl_listener new_output;
    struct wl_listener new_input;
    struct wl_listener new_xdg_client;
    struct wl_listener new_layer_client;
    struct wl_listener new_xwayland_client;
    struct wl_listener new_idle_inhibitor;
    struct wl_listener new_decoration;
    struct wl_listener new_lock;
    struct wl_listener new_virtual_keyboard;
    struct wl_listener xdg_activation_activate;
    struct wl_listener gamma_changed;
    struct wl_listener lock_manager_destroy;
    struct wl_listener output_layout_change;
    struct wl_listener output_manager_apply;
    struct wl_listener output_manager_test;
    struct wl_listener cursor_motion;
    struct wl_listener cursor_motion_absolute;
    struct wl_listener cursor_button;
    struct wl_listener cursor_axis;
    struct wl_listener cursor_frame;
    struct wl_listener cursor_shape_set_request;
    struct wl_listener seat_set_cursor;
    struct wl_listener seat_set_selection;
    struct wl_listener seat_set_primary_selection;
    struct wl_listener seat_set_request_start_drag;
    struct wl_listener seat_set_start_drag;
    struct wl_listener drag_icon_destroy;
    struct wl_listener xwayland_ready;
    struct wl_listener new_lock_surface;
    struct wl_listener lock_destroy;
    struct wl_listener unlock;
    struct wl_listener idle_inhibitor_destroy;
    struct wl_listener new_virtual_pointer;
    struct wl_listener new_shortcut_inhibitor;
    struct wl_listener shortcut_inhibitor_destroy;
    struct wl_listener new_pointer_constraint;
} listeners;

static const enum layer from_wly_layers[] = {
    [ZWLR_LAYER_SHELL_V1_LAYER_BACKGROUND] = BACKGROUND,
    [ZWLR_LAYER_SHELL_V1_LAYER_BOTTOM]     = BOTTOM,
	[ZWLR_LAYER_SHELL_V1_LAYER_TOP]        = TOP,
	[ZWLR_LAYER_SHELL_V1_LAYER_OVERLAY]    = OVERLAY,
};
static const struct wlr_drm_format argb32 = {.format = DRM_FORMAT_ARGB8888, .len = 1, .modifiers = (uint64_t[]){DRM_FORMAT_MOD_LINEAR}};
static const char broken[] = "broken"; // for clients with null titles / appids
static xcb_atom_t netatom[NetLast];
static struct wlr_surface *surface_grab = NULL;
static struct toplevel *toplevel_grab = NULL;
static uint32_t grab_x, grab_y;
static struct window *exclusive_focus = NULL;
static uint32_t bar_height = 0;
static bool shortcuts_inhibited = false;
static struct wlr_pointer_constraint_v1 *active_constraint = NULL;
static struct wl_event_source *stdin_source = NULL;
static char *status = NULL;

#include "config.h"

static_assert(length(tags) <= 32, "There must be less than 32 tags");
static_assert(length(layouts) != 0, "There must be at least 1 layout (if you don't want a tile like layout just set a NULL arrange function in a layout)");

void box_apply_bounds(struct wlr_box *box, struct wlr_box *bound) {
    // try and fit the box into the bounds if we can
    if (box->x >= bound->x + bound->width) box->x = bound->x + bound->width - box->width;
    if (box->y >= bound->y + bound->height) box->y = bound->y + bound->height - box->height;
    if (box->x + box->width <= bound->x) box->x = bound->x;
    if (box->y + box->height <= bound->y) box->y = bound->y;
}

bool buttonbind(uint32_t button) {
    if (shortcuts_inhibits_pointer && shortcuts_inhibited) return false;

    enum click_location location = CLICK_NONE;
    uint32_t mods = CLEAN_MOD_MASK(seat->keyboard_state.keyboard ? wlr_keyboard_get_modifiers(seat->keyboard_state.keyboard) : 0);

    uint32_t i = 0, n = i;
    double x, y;
    struct window *window = window_from_xy(cursor->x, cursor->y, &x, &y, NULL);
    if (window && window->type == WINDOW_TOPLEVEL) {
        location = CLICK_CLIENT;
    }
    else if (window && window->type == WINDOW_BAR && sel_mon->bar.scene->node.enabled) {
        uint32_t lrpad = font->height, status_width = text_width(font, status ? status : DEFAULT_STATUS) + 2;
        if (status_width > sel_mon->geometry.width) status_width = sel_mon->geometry.width;

        for (; i < length(tags) && x > (n += text_width(font, tags[i]) + lrpad); i++);
        if (i < length(tags)) {
            location = CLICK_TAG;
        }
        else if (x <= (n += text_width(font, sel_mon->lt_symbol) + lrpad)) {
            location = CLICK_LAYOUT;
        }
        else if (status_width < (sel_mon->geometry.width - n - lrpad) && x <= (n += sel_mon->geometry.width - status_width - n)) {
            location = CLICK_TITLE;
        }
        else if (x <= (n += n - status_width)) {
            location = CLICK_STATUS;
        }
    }

    for (const struct buttonbind *bind = buttonbinds; bind < END(buttonbinds); bind++) {
        if (button != bind->button || mods != bind->mods || location != bind->click || !bind->func) continue;

        union arg arg = bind->arg;
        if (location == CLICK_TAG && !arg.i) arg.ui = 1 << i;
        bind->func(&arg);

        return true;
    }

    return false;
}

void chvt(const union arg *arg) {
    // this checks for null by itself
    wlr_session_change_vt(session, arg->ui);
}

void cleanup(void) {
    wlr_xwayland_destroy(xwayland);
    xwayland = NULL;
    wl_display_destroy_clients(display);
    wlr_xcursor_manager_destroy(xcursor);
    wlr_output_layout_destroy(output_layout);
    wl_display_destroy(display);
    wlr_scene_node_destroy(&scene->tree.node);
    wl_event_source_remove(keyboards.key_repeat);
    wl_event_source_remove(virtual_keyboards.key_repeat);
    wl_event_source_remove(stdin_source);
    fcft_destroy(font);
    fcft_fini();
    free(status);
}

void client_notify_enter(struct wlr_surface *surface, struct wlr_keyboard *keyboard) {
    if (!surface) return;
    wlr_seat_keyboard_notify_enter(seat,
            surface,
            keyboard ? keyboard->keycodes : NULL,
            keyboard ? keyboard->num_keycodes : 0,
            keyboard ? &keyboard->modifiers : NULL);
}

void cursor_axis(struct wl_listener *listener, void *data) {
    struct wlr_pointer_axis_event *event = data;
    wlr_idle_notifier_v1_notify_activity(globals.idle_notifier, seat);
    wlr_seat_pointer_notify_axis(seat, event->time_msec, event->orientation, event->delta, event->delta_discrete, event->source);
}

void cursor_button(struct wl_listener *listener, void *data) {
    struct wlr_pointer_button_event *event = data;
    bool handled = false;

    wlr_idle_notifier_v1_notify_activity(globals.idle_notifier, seat);

    switch (event->state) {
        case WLR_BUTTON_PRESSED:
            cursor_mode = CURSOR_MODE_PRESSED;
            surface_grab = seat->pointer_state.focused_surface;
            if (locked) break;

            struct toplevel *toplevel = toplevel_from_xy(cursor->x, cursor->y, NULL, NULL);
            if (toplevel && (!toplevel_is_unmanaged(toplevel) || toplevel_wants_focus(toplevel))) toplevel_focus(toplevel, false);

            handled = buttonbind(event->button);

            break;
        case WLR_BUTTON_RELEASED:
            surface_grab = NULL;
            if (locked) {
                cursor_mode = CURSOR_MODE_RELEASED;
                break;
            }

            if (cursor_mode == CURSOR_MODE_MOVE || cursor_mode == CURSOR_MODE_RESIZE) {
                wlr_cursor_set_xcursor(cursor, xcursor, "default");
                sel_mon = monitor_from_xy(cursor->x, cursor->y);
                toplevel_set_monitor(toplevel_grab, sel_mon, 0);
            }

            cursor_mode = CURSOR_MODE_RELEASED;
            break;
    }

    if (handled) return;

    wlr_seat_pointer_notify_button(seat, event->time_msec, event->button, event->state);
}

void cursor_frame(struct wl_listener *listener, void *data) {
    wlr_seat_pointer_notify_frame(seat);
}

void cursor_motion(struct wl_listener *listener, void *data) {
    struct wlr_pointer_motion_event *event = data;
    cursor_motion_handle(&event->pointer->base, event->time_msec, event->delta_x, event->delta_y, event->unaccel_dx, event->unaccel_dy);
}

void cursor_motion_absolute(struct wl_listener *listener, void *data) {
    struct wlr_pointer_motion_absolute_event *event = data;

    double lx, ly;
    wlr_cursor_absolute_to_layout_coords(cursor, &event->pointer->base, event->x, event->y, &lx, &ly);

    double dx = lx - cursor->x,
           dy = ly - cursor->y;
    cursor_motion_handle(&event->pointer->base, event->time_msec, dx, dy, dx, dy);
}

void cursor_motion_handle(struct wlr_input_device *device, uint32_t time_msec, double dx, double dy, double dxa, double dya) {
    wlr_idle_notifier_v1_notify_activity(globals.idle_notifier, seat);

    wlr_relative_pointer_manager_v1_send_relative_motion(globals.relative_manager,
            seat, (uint64_t)time_msec*1000, dx, dy, dxa, dya);

    // TODO:
    // Handle new pointer contraints.
    // Handle their destruction.
    // And set them.
    // We should set a constraint as active if we focus a surface and said surface is a client and has a constraint for it.
    // We may also want to move some things around in the function.
    // We are probably going to to want to do things in this order:
    // 1. idle notify / set selected monitor.
    // 2. check cursor mode for resize / move.
    // 3. get the current surface / window.
    // 4. check for constraints.
    // 5. check for cursor mode pressed and surface grab stuff.
    // 6. toplevel focus.
    // 7. reset cursor if no surface.
    // 8. notify enter / motion.
    //
    // Think about moving the cursor and constraints and how we handle coordinates and stuff.
    //
    // dwl just iterates through all of the constraints and activates
    // (also deactivates previous ones basically culminating in the last constraint
    // in the list being active and the others being inactive).
    // I don't know why they do this it doesn't make sense.
    // Unless I misunderstand how this works this could cause us to set the wrong constraint
    // as active.

    // We need to do cursor move here for resize / move so that we can safely and accurately move the client.
    // But then we also need to do it again in the case we aren't.
    // Cause we need to change the differential for the cursor movement based on other properties.
    // I don't like this but I don't know how else to do this.
    // I mean we could do the constraint stuff before this and just use an if statement to guard
    // it in case we're resizing / moving and just do a wlr_cursor_move and selected monitor update at the same time.
    // But this feel like it doesn't make a whole lot of sense in the whole flow of the function so I'm hesitant.
    if (cursor_mode == CURSOR_MODE_RESIZE || cursor_mode == CURSOR_MODE_MOVE) {
        wlr_cursor_move(cursor, device, dx, dy);
        monitor_update_selected();
    }
    switch (cursor_mode) {
        case CURSOR_MODE_MOVE:
            toplevel_resize(toplevel_grab,
                    &(struct wlr_box){
                    .x = cursor->x - grab_x, .y = cursor->y - grab_y,
                    .width = toplevel_grab->geometry.width, .height = toplevel_grab->geometry.height,
                    },
                    &scene_geometry);
            return;
        case CURSOR_MODE_RESIZE:
            toplevel_resize(toplevel_grab,
                    &(struct wlr_box){
                    .x = toplevel_grab->geometry.x, .y = toplevel_grab->geometry.y,
                    .width = cursor->x - toplevel_grab->geometry.x, .height = cursor->y - toplevel_grab->geometry.y,
                    },
                    &scene_geometry);
            return;
        default:
            break;
    }

    if (seat->drag) wlr_scene_node_set_position(&drag_icon->node, cursor->x, cursor->y);

    double x, y;
    struct toplevel *toplevel = NULL;
    if (active_constraint &&
            active_constraint->surface == seat->pointer_state.focused_surface &&
            client_from_surface(active_constraint->surface, NULL, NULL, &toplevel) &&
            toplevel) {
        x = cursor->x - toplevel->geometry.x - toplevel->border_width;
        y = cursor->y - toplevel->geometry.y - toplevel->border_width;
        double xcu, ycu;
        if (wlr_region_confine(&active_constraint->region,
                    x, y,
                    x + dx, y + dy,
                    &xcu, &ycu)) {
            dx = xcu - x;
            dy = ycu - y;
        }

        if (active_constraint->type == WLR_POINTER_CONSTRAINT_V1_LOCKED) return;
    }

    wlr_cursor_move(cursor, device, dx, dy);
    monitor_update_selected();

    struct wlr_surface *surface = NULL;
    struct window *window = window_from_xy(cursor->x, cursor->y, &x, &y, &surface);
    struct layered *layered = layered_try_from_window(window);
    toplevel = toplevel_try_from_window(window);
    if (cursor_mode == CURSOR_MODE_PRESSED &&
            !seat->drag &&
            surface != surface_grab &&
            client_from_surface(surface_grab, NULL, &layered, &toplevel)) {
        surface = surface_grab;
        x = cursor->x - (layered ? layered->geometry.x : toplevel->geometry.x);
        y = cursor->y - (layered ? layered->geometry.y : toplevel->geometry.y);
    }

    if (toplevel && !toplevel_is_unmanaged(toplevel)) toplevel_focus(toplevel, false);

    if (!surface) {
        if (!seat->drag) wlr_cursor_set_xcursor(cursor, xcursor, "default");
        wlr_seat_pointer_clear_focus(seat);
        return;
    }

    wlr_seat_pointer_notify_enter(seat, surface, x, y);
    wlr_seat_pointer_notify_motion(seat, time_msec, x, y);

    // After a constraint is destroyed and we warp the cursor to the hint we
    // this event will get called and cause a problem with zombie constraints.
    struct wlr_pointer_constraint_v1 *constraint = wlr_pointer_constraints_v1_constraint_for_surface(globals.pointer_constraints, surface, seat);
    pointer_constrain(constraint && constraint->data ? constraint->data : NULL);
}

void cursor_shape_set_request(struct wl_listener *listener, void *data) {
    struct wlr_cursor_shape_manager_v1_request_set_shape_event *event = data;
    if (seat->pointer_state.focused_client == event->seat_client) wlr_cursor_set_xcursor(cursor, xcursor, wlr_cursor_shape_v1_name(event->shape));
}

void decoration_destroy(struct wl_listener *listener, void *data) {
    struct toplevel *toplevel = wl_container_of(listener, toplevel, xdg.listeners.decoration_destroy);
    wl_list_remove(&toplevel->xdg.listeners.decoration_destroy.link);
    wl_list_remove(&toplevel->xdg.listeners.decoration_request.link);
}

void decoration_request(struct wl_listener *listener, void *data) {
    struct toplevel *toplevel = wl_container_of(listener, toplevel, xdg.listeners.decoration_request);
    wlr_xdg_toplevel_decoration_v1_set_mode(toplevel->xdg.decoration, WLR_XDG_TOPLEVEL_DECORATION_V1_MODE_SERVER_SIDE);
}

void drag_icon_destroy(struct wl_listener *listener, void *data) {
    /* TODO: Focus enter isn't sent during drag, so refocus the focused node. */
    toplevel_focus(monitor_get_top(sel_mon), false);
    wl_list_remove(&listener->link);
}

void draw_bar(struct wl_listener *listener, void *unused_data) {
    struct monitor *monitor = wl_container_of(listener, monitor, listeners.frame_done);
    const char *status_str = status ? status : DEFAULT_STATUS;
    const uint32_t height = monitor->bar.geometry.height,
          width = monitor->geometry.width,
          boxs = font->height / 9,
          boxw = font->height / 6 + 1,
          bottom_pad = (height - font->height) / 2 + font->ascent,
          lrpad = font->height;
    enum color_scheme scheme;
    pixman_color_t background, foreground;
    struct wlr_buffer *buffer = swapchain_get(&monitor->bar.swapchain, width, height);
    if (!buffer) return;

    void *data;
    size_t stride;
    uint32_t format;
    wlr_buffer_begin_data_ptr_access(buffer, WLR_BUFFER_DATA_PTR_ACCESS_WRITE, &data, &format, &stride);

    pixman_image_t *image = pixman_image_create_bits(PIXMAN_a8r8g8b8, width, height, data, stride);

    uint32_t status_width = 0;
    if (monitor == sel_mon) {
        background = PIXMAN_COLOR(colors[SCHEME_NORMAL][COLOR_BACKGROUND]);
        foreground = PIXMAN_COLOR(colors[SCHEME_NORMAL][COLOR_FOREGROUND]);

        status_width = text_width(font, status_str) + 2;
        if (status_width > width) status_width = width;

        pixman_image_fill_boxes(PIXMAN_OP_SRC, image, &background, 1,
                &(pixman_box32_t){
                .x1 = width - status_width, .x2 = width,
                .y1 = 0, .y2 = height,
                });

        text_draw(font, image, status_str, &foreground, &(struct wlr_box){
                .x = width - status_width, .y = bottom_pad,
                .width = status_width, .height = -1,
                });
    }

    struct toplevel *toplevel;
    uint32_t occupied = 0, urgent = 0, tagset = monitor->tagset[monitor->sel_tags], x = 0;
    wl_list_for_each(toplevel, &monitor->toplevels, link) {
        occupied |= toplevel->tags;
        if (toplevel->urgent) urgent |= toplevel->tags;
    }

    // tags
    for (int i = 0; i < length(tags); i++) {
        uint32_t tag_width = text_width(font, tags[i]) + lrpad;
        scheme = (urgent & 1 << i ? SCHEME_URGENT : (tagset & 1 << i ? SCHEME_SELECTED : SCHEME_NORMAL));
        background = PIXMAN_COLOR(colors[scheme][COLOR_BACKGROUND]), foreground = PIXMAN_COLOR(colors[scheme][COLOR_FOREGROUND]);
        if (x + tag_width >= width) goto done;

        pixman_image_fill_boxes(PIXMAN_OP_SRC, image, &background, 1,
                &(pixman_box32_t){
                .x1 = x, .x2 = x + tag_width,
                .y1 = 0, .y2 = height,
                });

        if (occupied & 1 << i) {
            pixman_image_fill_boxes(PIXMAN_OP_SRC, image, &foreground, 1,
                &(pixman_box32_t){
                .x1 = x + boxs, .x2 = x + boxs + boxw,
                .y1 = boxs, .y2 = boxs + boxw,
                });

            if (sel_mon != monitor || !sel_mon->sel || !(sel_mon->sel->tags & 1 << i)) {
                pixman_image_fill_boxes(PIXMAN_OP_SRC, image, &background, 1,
                        &(pixman_box32_t){
                        .x1 = x + boxs + 1, .x2 = x + boxs + boxw - 1,
                        .y1 = boxs + 1, .y2 = boxs + boxw - 1,
                        });
            }
        }

        x = text_draw(font, image, tags[i], &foreground,
                &(struct wlr_box){
                .x = x + (lrpad / 2), .y = bottom_pad,
                .width = -1, .height = -1,
                }) + (lrpad / 2);
    }

    // layout
    background = PIXMAN_COLOR(colors[SCHEME_NORMAL][COLOR_BACKGROUND]);
    foreground = PIXMAN_COLOR(colors[SCHEME_NORMAL][COLOR_FOREGROUND]);
    uint32_t layout_width = text_width(font, monitor->lt_symbol) + lrpad;
    if (x + layout_width >= width) goto done;

    pixman_image_fill_boxes(PIXMAN_OP_SRC, image, &background, 1,
            &(pixman_box32_t){
            .x1 = x, .x2 = x + layout_width,
            .y1 = 0, .y2 = height,
            });

    x = text_draw(font, image, monitor->lt_symbol, &foreground,
            &(struct wlr_box){
            .x = x + (lrpad / 2), .y = bottom_pad,
            .width = -1, .height = -1,
            }) + (lrpad / 2);

    // title
    scheme = monitor == sel_mon && monitor->sel ? SCHEME_SELECTED : SCHEME_NORMAL;
    background = PIXMAN_COLOR(colors[scheme][COLOR_BACKGROUND]);
    foreground = PIXMAN_COLOR(colors[scheme][COLOR_FOREGROUND]);

    if (status_width > width - x - lrpad) goto done;
    uint32_t title_width = width - status_width - x;

    pixman_image_fill_boxes(PIXMAN_OP_SRC, image, &background, 1,
            &(pixman_box32_t){
            .x1 = x, .x2 = x + title_width,
            .y1 = 0, .y2 = height,
            });

    if (monitor->sel && monitor->sel->floating) {
        pixman_image_fill_boxes(PIXMAN_OP_SRC, image, &foreground, 1,
                &(pixman_box32_t){
                .x1 = x + boxs, .x2 = x + boxs + boxw,
                .y1 = boxs, .y2 = boxs + boxw,
                });

        if (!toplevel_wants_floating(monitor->sel)) {
            pixman_image_fill_boxes(PIXMAN_OP_SRC, image, &background, 1,
                    &(pixman_box32_t){
                    .x1 = x + boxs + 1, .x2 = x + boxs + boxw - 1,
                    .y1 = boxs + 1, .y2 = boxs + boxw - 1,
                    });
        }
    }

    x = text_draw(font, image, monitor->sel ? toplevel_get_title(monitor->sel) : NULL, &foreground,
            &(struct wlr_box){
            .x = x + (lrpad / 2), .y = bottom_pad,
            .width = title_width - (lrpad / 2), .height = -1,
            });

done:
    wlr_buffer_end_data_ptr_access(buffer);
    wlr_scene_buffer_set_buffer(monitor->bar.scene, buffer);
    swapchain_buffer_submitted(&monitor->bar.swapchain, buffer);
    pixman_image_unref(image);
}

void focus_stack(const union arg *arg) {
    struct toplevel *toplevel, *current = monitor_get_top(sel_mon);
    if (!current || !current->monitor || (current->fullscreen && toplevel_has_children(current))) return;

    // Get the very next element in the list based on the arg.
    if (arg->i > 0) {
        wl_list_for_each(toplevel, &current->link, link) {
            if (&toplevel->link == &current->monitor->toplevels) continue;
            if (CLIENT_VISIBLE_ON(toplevel, sel_mon)) break;
        }
    }
    else {
        wl_list_for_each_reverse(toplevel, &current->link, link) {
            if (&toplevel->link == &current->monitor->toplevels) continue;
            if (CLIENT_VISIBLE_ON(toplevel, sel_mon)) break;
        }
    }

    toplevel_focus(toplevel, true);
}

void gamma_changed(struct wl_listener *listener, void *data) {
    struct wlr_gamma_control_manager_v1_set_gamma_event *event = data;
    struct monitor *monitor = event->output->data;
    if (!monitor) return;
    monitor->gamma_changed = true;
    wlr_output_schedule_frame(monitor->output);
}

void idle_inhibitor_destroy(struct wl_listener *listener, void *data) {
    wl_list_remove(&listener->link);

    // If there is only one inhibitor then uninhibited and return
    // otherwise find the next inhibitor to listen for destructon
    struct wlr_idle_inhibitor_v1 *inhibitor;
    wl_list_for_each(inhibitor, &globals.idle_inhibit->inhibitors, link) {
        if (inhibitor->surface == data) {
            if (inhibitor->link.next == &globals.idle_inhibit->inhibitors) {
                wlr_idle_notifier_v1_set_inhibited(globals.idle_notifier, false);
                return;
            }

            continue;
        }

        listen(&inhibitor->events.destroy, &listeners.idle_inhibitor_destroy, idle_inhibitor_destroy);
        break;
    }
}

bool keybind(struct keyboard_group *group, const xkb_keysym_t keysym, uint32_t mods) {
    if (shortcuts_inhibited) return false;
    mods = CLEAN_MOD_MASK(mods);

    for (int i = 0; i < length(keybinds); i++) {
        const struct keybind *bind = &keybinds[i];
        if (!bind->func || CLEAN_MOD_MASK(bind->mods) != mods || bind->keysym != keysym) continue;


        bind->func(&bind->arg);
        return true;
    }

    return false;
}

void keyboard_destroy(struct wl_listener *listener, void *data) {
    struct keyboard *keyboard = wl_container_of(listener, keyboard, listeners.destroy);
    wl_list_remove(&keyboard->listeners.destroy.link);
    wl_list_remove(&keyboard->link);
    if (wl_list_empty(&keyboard->group->keyboards)) seat_update_capabilities();
    free(keyboard);
}

void keyboard_init(struct keyboard_group *group, struct wlr_keyboard *wlr_keyboard) {
    if (!group || !wlr_keyboard) return;

    wlr_keyboard_set_keymap(wlr_keyboard, group->group->keyboard.keymap);
    wlr_keyboard_group_add_keyboard(group->group, wlr_keyboard);

    struct keyboard *keyboard = ezalloc(sizeof(*keyboard));
    keyboard->keyboard = wlr_keyboard;
    keyboard->group = group;
    listen(&keyboard->keyboard->base.events.destroy, &keyboard->listeners.destroy, keyboard_destroy);
    wl_list_insert(&group->keyboards, &keyboard->link);
}

void keyboard_key(struct wl_listener *listener, void *data) {
    struct keyboard_group *group = wl_container_of(listener, group, listeners.key);
    struct wlr_keyboard_key_event *event = data;

    wlr_idle_notifier_v1_notify_activity(globals.idle_notifier, seat);

    bool handled = false;
    uint32_t keycode = event->keycode + 8; // libinput keycode -> xkbcommon
    const xkb_keysym_t *syms;
    uint32_t syms_len = xkb_state_key_get_syms(group->group->keyboard.xkb_state, keycode, &syms);
    uint32_t mods = wlr_keyboard_get_modifiers(&group->group->keyboard);

    if (!locked && event->state == WL_KEYBOARD_KEY_STATE_PRESSED) {
        for (int i = 0; i < syms_len && !handled; i++) {
            handled = keybind(group, syms[i], mods);
        }
    }

    if (handled && group->group->keyboard.repeat_info.delay > 0) {
        group->keysyms = syms;
        group->keysyms_len = syms_len;
        group->mods = mods;
        wl_event_source_timer_update(group->key_repeat, group->group->keyboard.repeat_info.delay);
    } else {
        wl_event_source_timer_update(group->key_repeat, 0);
    }

    if (handled) return;

    wlr_seat_set_keyboard(seat, &group->group->keyboard);
    wlr_seat_keyboard_notify_key(seat, event->time_msec, event->keycode, event->state);
}

void keyboard_modifiers(struct wl_listener *listener, void *data) {
    struct keyboard_group *keyboard = wl_container_of(listener, keyboard, listeners.modifiers);

    // NOTE: dwl does not do this but I think it counts
    wlr_idle_notifier_v1_notify_activity(globals.idle_notifier, seat);

    wlr_seat_set_keyboard(seat, &keyboard->group->keyboard);
    wlr_seat_keyboard_notify_modifiers(seat, &keyboard->group->keyboard.modifiers);
}

int32_t keyboard_repeat(void *data) {
    struct keyboard_group *group = data;
    if (!group->keysyms_len || group->group->keyboard.repeat_info.rate <= 0) return 0;

    wl_event_source_timer_update(group->key_repeat, 1000 / group->group->keyboard.repeat_info.rate);

    for (int i = 0; i < group->keysyms_len; i++) keybind(group, group->keysyms[i], group->mods);

    return 0;
}

void layered_commit(struct wl_listener *listener, void *data) {
    struct layered *layered = wl_container_of(listener, layered, listeners.commit);
    struct wlr_layer_surface_v1 *surface = layered->surface;

    if (!surface->current.committed) return;

    struct wlr_scene_tree *scene = layers[from_wly_layers[surface->current.layer]];
    if (scene != layered->scene->tree->node.parent) {
        wlr_scene_node_reparent(&layered->scene->tree->node, scene);
        wl_list_remove(&layered->link);
        wl_list_insert(&layered->monitor->layers[layered->surface->current.layer], &layered->link);
        wlr_scene_node_reparent(&layered->popups->node, layered->surface->pending.layer < ZWLR_LAYER_SHELL_V1_LAYER_TOP ?
            layers[TOP] : scene);
    }

    monitor_layers_arrange(layered->monitor);
}

void layered_destroy(struct wl_listener *listener, void *data) {
    struct layered *layered = wl_container_of(listener, layered, listeners.destroy);
    wl_list_remove(&layered->link);
    wl_list_remove(&layered->listeners.commit.link);
    wl_list_remove(&layered->listeners.destroy.link);
    wl_list_remove(&layered->listeners.map.link);
    wl_list_remove(&layered->listeners.unmap.link);
    wlr_scene_node_destroy(&layered->popups->node);
    free(layered);
}

void layered_map(struct wl_listener *listener, void *data) {
    struct layered *layered = wl_container_of(listener, layered, listeners.map);
    monitor_layers_arrange(layered->monitor);
}

struct layered *layered_try_from_window(struct window *window) {
    if (!window || window->type != WINDOW_LAYERED) return NULL;

    struct layered *layered = wl_container_of(window, layered, base);
    return layered;
}

void layered_unmap(struct wl_listener *listener, void *data) {
    struct layered *layered = wl_container_of(listener, layered, listeners.unmap);
    monitor_layers_arrange(layered->monitor);
    if (&layered->base == exclusive_focus) exclusive_focus = NULL;
    if (layered->surface->surface == seat->keyboard_state.focused_surface) toplevel_focus(monitor_get_top(sel_mon), true);
}

void lock_destroy() {
    wl_list_remove(&listeners.lock_destroy.link);
    wl_list_remove(&listeners.new_lock_surface.link);
    wl_list_remove(&listeners.unlock.link);
    wlr_cursor_set_xcursor(cursor, xcursor, "default");
    wlr_scene_node_set_enabled(&session_lock.surfaces->node, false);
    wlr_scene_node_set_enabled(&session_lock.background->node, false);
    session_lock.lock = NULL;
    locked = false;
    // TODO: Damage whole of all outputs?
}

void lock_manager_destroy(struct wl_listener *listener, void *data) {
    wl_list_remove(&listeners.lock_manager_destroy.link);
    wl_list_remove(&listeners.new_lock.link);
}

void lock_surface_destroy(struct wl_listener *listener, void *data) {
    struct monitor *monitor = wl_container_of(listener, monitor, listeners.lock_surface_destroy);
    struct wlr_session_lock_surface_v1 *surface = monitor->lock_surface;

    wl_list_remove(&listener->link);
    monitor->lock_surface = NULL;

    if (surface->surface != seat->keyboard_state.focused_surface) return;

    // Refocus the to another lock surface. Otherwise clear focus.
    if (locked && session_lock.lock && !wl_list_empty(&session_lock.lock->surfaces)) {
        surface = wl_container_of(session_lock.lock->surfaces.next, surface, link);
        client_notify_enter(surface->surface, wlr_seat_get_keyboard(seat));
    }
    else if (!locked) {
        toplevel_focus(monitor_get_top(sel_mon), true);
    }
    else {
        wlr_seat_keyboard_clear_focus(seat);
    }

    // TODO: Damage whole output?
}

void master_set_amount(const union arg *arg) {
    sel_mon->master_amount = MAX((int32_t)sel_mon->master_amount + arg->i, 0);
    monitor_arrange(sel_mon);
}

void master_set_factor(const union arg *arg) {
    sel_mon->master_factor = MAX(sel_mon->master_factor + arg->f, 0);
    monitor_arrange(sel_mon);
}

void monitor_arrange(struct monitor *monitor) {
    if (!monitor || !monitor->output->enabled) return;

    const struct layout *layout = monitor->layouts[monitor->layout];
    struct toplevel *toplevel;
    wl_list_for_each(toplevel, &monitor->toplevels, link) {
        wlr_scene_node_set_enabled(&toplevel->scene->node, CLIENT_VISIBLE_ON(toplevel, monitor));
        toplevel_set_suspended(toplevel, !CLIENT_VISIBLE_ON(toplevel, monitor));
    }

    toplevel = monitor_get_top(monitor);
    wlr_scene_node_set_enabled(&monitor->fullscreen_background->node, toplevel && toplevel->fullscreen);

    if (layout->arrange) layout->arrange(monitor);
}

void monitor_cleanup(struct monitor *monitor) {
    if (!monitor) return;

    if (monitor == sel_mon) {
        sel_mon = NULL;
        struct monitor *tmp;
        wl_list_for_each(tmp, &monitors, link) {
            if (tmp == monitor || !tmp->output->enabled) continue;

            sel_mon = tmp;
            break;
        }
    }

    monitor->geometry = monitor->toplevel_space = (struct wlr_box){0};
    if (!sel_mon) return;

    struct toplevel *toplevel, *tmp;
    wl_list_for_each_safe(toplevel, tmp, &monitor->toplevels, link) {
        toplevel_set_monitor(toplevel, sel_mon, toplevel->tags);
    };
    monitor->sel = NULL;
}

void monitor_destroy(struct wl_listener *listener, void *data) {
    struct monitor *monitor = wl_container_of(listener, monitor, listeners.destroy);

    for (int i = 0; i < length(monitor->layers); i++) {
        struct layered *layered;
        wl_list_for_each(layered, &monitor->layers[i], link) wlr_layer_surface_v1_destroy(layered->surface);
    }

    wl_list_remove(&monitor->listeners.frame.link);
    wl_list_remove(&monitor->listeners.destroy.link);
    wl_list_remove(&monitor->listeners.request_state.link);
    wl_list_remove(&monitor->listeners.frame_done.link);
    wl_list_remove(&monitor->link);
    wlr_output_layout_remove(output_layout, monitor->output);
    wlr_scene_output_destroy(monitor->output_scene);
    wlr_scene_node_destroy(&monitor->bar.scene->node);
    wlr_allocator_destroy(monitor->bar.swapchain.allocator);
    swapchain_destroy(&monitor->bar.swapchain);
    monitor_cleanup(monitor);
    free(monitor);
}

struct monitor *monitor_enabled_in_direction(struct monitor *monitor, enum wlr_direction direction) {
    uint32_t n = wl_list_length(&monitors), i = 0;
    if (!n) return NULL;

    do {
        if (!wlr_output_layout_get(output_layout, monitor->output)) return NULL;

        struct wlr_output *next = wlr_output_layout_adjacent_output(output_layout, direction, monitor->output, monitor->geometry.x, monitor->geometry.y);
        if (next) {
            monitor = next->data;
            continue;
        }

        next = wlr_output_layout_farthest_output(output_layout, direction ^ (WLR_DIRECTION_LEFT|WLR_DIRECTION_RIGHT), monitor->output, monitor->geometry.x, monitor->geometry.y);
        if (next) {
            monitor = next->data;
            continue;
        }

        return NULL;
    } while (!monitor->output->enabled && i++ < n);

    return i < n ? monitor : NULL;
}

void monitor_focus(const union arg *arg) {
    struct monitor *monitor = monitor_enabled_in_direction(sel_mon, arg->ui);
    if (monitor == sel_mon || !monitor) return;

    toplevel_unfocus(sel_mon->sel);
    sel_mon = monitor;
    toplevel_focus(monitor_get_top(sel_mon), false);
}

void monitor_frame(struct wl_listener *listener, void *data) {
    struct timespec now;
    struct monitor *monitor = wl_container_of(listener, monitor, listeners.frame);

    // NOTE: This isn't really needed as it's just causing problems on top of not actually fixing the problem that it wants to fix
    // See: https://github.com/djpohly/dwl/issues/306
	/* Render if no XDG clients have an outstanding resize and are visible on this monitor. */
    //struct toplevel *toplevel;
    //wl_list_for_each(toplevel, &toplevels, link) {
    //    if (toplevel->resize_serial && !toplevel->floating && toplevel_is_rendered(toplevel, monitor) && !toplevel_is_stopped(toplevel) ) {
    //        //wlr_log(WLR_DEBUG, "skipping commit");
    //        goto skip;
    //    }
    //}

    // Since we want to apply gamma changes during the scene output commit we essentially need to do wlr_scene_output_commit ourselves.
    if (!monitor->output->needs_frame && !pixman_region32_not_empty(&monitor->output_scene->damage_ring.current)) goto frame;

    struct wlr_output_state state;
    wlr_output_state_init(&state);

    // TODO: Test this. I don't know what I'm doing here
    if (monitor->gamma_changed) {
        struct wlr_output_state pending;
        wlr_output_state_init(&pending);

        monitor->gamma_changed = false;
        struct wlr_gamma_control_v1 *control = wlr_gamma_control_manager_v1_get_control(globals.gamma_control, monitor->output);
        if (wlr_gamma_control_v1_apply(control, &pending) && wlr_output_test_state(monitor->output, &pending)) {
            wlr_output_state_copy(&state, &pending);
        }
        else {
            wlr_gamma_control_v1_send_failed_and_destroy(control);
        }

        wlr_output_state_finish(&pending);
    }

    if (wlr_scene_output_build_state(monitor->output_scene, &state, NULL) && wlr_output_commit_state(monitor->output, &state)) {
        wlr_damage_ring_rotate(&monitor->output_scene->damage_ring);
    }

    wlr_output_state_finish(&state);

frame:
    if (!clock_gettime(CLOCK_MONOTONIC, &now)) {
        wlr_scene_output_send_frame_done(monitor->output_scene, &now);
    }
}

struct monitor *monitor_from_xy(uint32_t x, uint32_t y) {
    struct wlr_output *output = wlr_output_layout_output_at(output_layout, x, y);
    return output ? output->data : NULL;
}

struct toplevel *monitor_get_top(struct monitor *monitor) {
    if (!monitor) return NULL;

    struct toplevel *toplevel;
    wl_list_for_each(toplevel, &monitor->stack, stack_link) {
        if (CLIENT_VISIBLE_ON(toplevel, monitor)) return toplevel;
    }

    return NULL;
}

void monitor_layers_arrange(struct monitor *monitor) {
    if (!monitor || !monitor->output->enabled) return;

    struct wlr_box usable = monitor->geometry;
    for (int i = WLR_LAYER_SHELL_LAYERS - 1; i >= 0; i--) monitor_layer_arrange(monitor, i, &usable, true);

    if (monitor->bar.scene->node.enabled && wlr_box_equal(&usable, &monitor->geometry)) {
        if (top_bar) usable.y += monitor->bar.geometry.height;
        usable.height -= monitor->bar.geometry.height;
    }
    if (!wlr_box_equal(&usable, &monitor->toplevel_space)) {
        struct wlr_box intersect;
        if (monitor->bar.scene->node.enabled && wlr_box_intersection(&intersect, &usable, &monitor->bar.geometry)) {
            usable.y = top_bar ? usable.y + intersect.height : usable.y - intersect.height;
            usable.height -= intersect.height;
        }
        monitor->toplevel_space = usable;
        monitor_arrange(monitor);
    }

    for (int i = WLR_LAYER_SHELL_LAYERS - 1; i >= 0; i--) monitor_layer_arrange(monitor, i, &usable, false);

    const uint32_t layers_above_shell[] = {
        ZWLR_LAYER_SHELL_V1_LAYER_TOP,
        ZWLR_LAYER_SHELL_V1_LAYER_OVERLAY,
    };
    for (int i = 0; i < length(layers_above_shell); i++) {
        struct layered *layered;
        wl_list_for_each_reverse(layered, &monitor->layers[layers_above_shell[i]], link) {
            // During an initial commit / before the first ack_configure
            // the client might want keyboard focus but we want to make sure
            // we don't hand out keyboard focus to clients who have been unmapped
            // and not configured
            if (locked || !layered->surface->current.keyboard_interactive || (!layered->surface->surface->mapped && !layered->surface->configured)) continue;

            toplevel_unfocus(sel_mon ? sel_mon->sel : NULL);
            exclusive_focus = &layered->base;
            client_notify_enter(layered->surface->surface, wlr_seat_get_keyboard(seat));
            return;
        }
    }
}

void monitor_layer_arrange(struct monitor *monitor, enum zwlr_layer_shell_v1_layer layer, struct wlr_box *usable, bool exclusive) {
    struct wlr_box full = monitor->geometry;
    struct layered *layered;
    wl_list_for_each(layered, &monitor->layers[layer], link) {
        if (exclusive != (layered->surface->current.exclusive_zone > 0)) {
            continue;
        }

        wlr_scene_layer_surface_v1_configure(layered->scene, &full, usable);

        layered->geometry.x = layered->scene->tree->node.x;
        layered->geometry.y = layered->scene->tree->node.y;
        wlr_scene_node_set_position(&layered->popups->node, layered->geometry.x, layered->geometry.y);
    }
}

void monitor_request_state(struct wl_listener *listener, void *data) {
    struct wlr_output_event_request_state *event = data;
    wlr_output_commit_state(event->output, event->state);
}

void monitor_tag(const union arg *arg) {
    struct toplevel *toplevel = monitor_get_top(sel_mon);
    struct monitor *monitor = monitor_enabled_in_direction(sel_mon, arg->ui);
    if (monitor == sel_mon || !monitor) return;

    if (toplevel) toplevel_set_monitor(toplevel, monitor, 0);
}

void monitor_update_config(struct monitor *monitor, struct wlr_output_configuration_v1 *config) {
    if (!monitor || !config) return;

    struct wlr_output_configuration_head_v1 *head = wlr_output_configuration_head_v1_create(config, monitor->output);
    if (!monitor->output->enabled) return;

    head->state.x = monitor->geometry.x;
    head->state.y = monitor->geometry.y;
}

void monitor_update_geometry(struct monitor *monitor) {
    if (!monitor || !monitor->output->enabled) return;

    struct wlr_box geometry;
    wlr_output_layout_get_box(output_layout, monitor->output, &geometry);
    if (wlr_box_empty(&geometry)) {
        monitor->geometry = geometry;
        return;
    }

    wlr_scene_output_set_position(monitor->output_scene, geometry.x, geometry.y);

    wlr_scene_rect_set_size(monitor->fullscreen_background, geometry.width, geometry.height);
    wlr_scene_node_set_position(&monitor->fullscreen_background->node, geometry.x, geometry.y);

    monitor->bar.geometry = (struct wlr_box){0};
    if (monitor->bar.scene->node.enabled) {
        monitor->bar.geometry.x = geometry.x;
        monitor->bar.geometry.y = top_bar ? geometry.y : geometry.height - bar_height;
        monitor->bar.geometry.width = geometry.width;
        monitor->bar.geometry.height = bar_height;
        wlr_scene_node_set_position(&monitor->bar.scene->node, monitor->bar.geometry.x, monitor->bar.geometry.y);
        wlr_scene_buffer_set_dest_size(monitor->bar.scene, monitor->bar.geometry.width, monitor->bar.geometry.height);
    }

    monitor->geometry = geometry;
    monitor_layers_arrange(monitor);
}

void monitor_update_selected(void) {
    struct monitor *monitor = monitor_from_xy(cursor->x, cursor->y);
    if (monitor == sel_mon) return;

    toplevel_unfocus(sel_mon->sel);
    sel_mon = monitor;
    toplevel_focus(monitor_get_top(sel_mon), false);
}

void monocle(struct monitor *monitor) {
    uint32_t n = 0;
    struct toplevel *toplevel;
    wl_list_for_each(toplevel, &monitor->toplevels, link) {
        if (!CLIENT_VISIBLE_ON(toplevel, monitor) || toplevel->floating || toplevel->fullscreen) continue;

        toplevel_resize(toplevel, &monitor->toplevel_space, &monitor->toplevel_space);
        n++;
    }

    if (n) snprintf(monitor->lt_symbol, LAYOUT_SYMBOL_MAX, "[%d]", n);
    else strncpy(monitor->lt_symbol, monitor->layouts[monitor->layout]->symbol, LAYOUT_SYMBOL_MAX);
    if ((toplevel = monitor_get_top(monitor))) wlr_scene_node_raise_to_top(&toplevel->scene->node);
}

void move_resize(const union arg *arg) {
    if ((cursor_mode != CURSOR_MODE_PRESSED && cursor_mode != CURSOR_MODE_RELEASED) || (arg->ui != CURSOR_MODE_MOVE && arg->ui != CURSOR_MODE_RESIZE)) return;

    toplevel_grab = toplevel_from_xy(cursor->x, cursor->y, NULL, NULL);
    if (!toplevel_grab || toplevel_is_unmanaged(toplevel_grab) || toplevel_grab->fullscreen) return;

    toplevel_set_floating(toplevel_grab, true);
    wlr_scene_node_raise_to_top(&toplevel_grab->scene->node);
    switch (cursor_mode = arg->ui) {
        case CURSOR_MODE_MOVE:
            grab_x = cursor->x - toplevel_grab->geometry.x;
            grab_y = cursor->y - toplevel_grab->geometry.y;
            wlr_cursor_set_xcursor(cursor, xcursor, "fleur");
            break;
        case CURSOR_MODE_RESIZE:
            /* Doesn't work for X11 output - the next absolute motion event returns the cursor to where it started */
            wlr_cursor_warp_closest(cursor, NULL,
                    toplevel_grab->geometry.x + toplevel_grab->geometry.width,
                    toplevel_grab->geometry.y + toplevel_grab->geometry.height);
            wlr_cursor_set_xcursor(cursor, xcursor, "se-resize");
            break;
        default:
            return;
    }
}

void new_decoration(struct wl_listener *listener, void *data) {
    struct wlr_xdg_toplevel_decoration_v1 *decoration = data;
    struct toplevel *toplevel = decoration->toplevel->base->data;
    toplevel->xdg.decoration = decoration;

    listen(&decoration->events.request_mode, &toplevel->xdg.listeners.decoration_request, decoration_request);
    listen(&decoration->events.destroy, &toplevel->xdg.listeners.decoration_destroy, decoration_destroy);

    decoration_request(&toplevel->xdg.listeners.decoration_request, decoration);
}

void new_idle_inhibitor(struct wl_listener *listener, void *data) {
    struct wlr_idle_inhibitor_v1 *inhibitor = data;
    listen(&inhibitor->events.destroy, &listeners.idle_inhibitor_destroy, idle_inhibitor_destroy);
}

void new_input(struct wl_listener *listener, void *data) {
    struct wlr_input_device *device = data;
    switch (device->type) {
        case WLR_INPUT_DEVICE_KEYBOARD:
            keyboard_init(&keyboards, wlr_keyboard_from_input_device(device));
            break;
        case WLR_INPUT_DEVICE_POINTER:
            pointer_init(wlr_pointer_from_input_device(device));
            break;
        default:
            // TODO: The other ones.
            break;
    }

    seat_update_capabilities();
}

void new_layer_client(struct wl_listener *listener, void *data) {
    struct wlr_layer_surface_v1 *surface = data;
    struct wlr_scene_tree *layer = layers[from_wly_layers[surface->pending.layer]];

    if (!surface->output) {
        if (!sel_mon) {
            wlr_layer_surface_v1_destroy(surface);
            return;
        }
        surface->output = sel_mon->output;
    }

    struct layered *layered = ezalloc(sizeof(*layered));
    layered->base.type = WINDOW_LAYERED;
    layered->surface = surface;
    layered->monitor = surface->output->data;
    layered->scene = wlr_scene_layer_surface_v1_create(layer, surface);
    layered->popups = wlr_scene_tree_create(layered->surface->pending.layer < ZWLR_LAYER_SHELL_V1_LAYER_TOP ?
            layers[TOP] : layer);
    layered->scene->tree->node.data = layered->popups->node.data = &layered->base;

    listen(&layered->surface->events.destroy, &layered->listeners.destroy, layered_destroy);
    listen(&layered->surface->surface->events.map, &layered->listeners.map, layered_map);
    listen(&layered->surface->surface->events.unmap, &layered->listeners.unmap, layered_unmap);
    listen(&layered->surface->surface->events.commit, &layered->listeners.commit, layered_commit);

    wl_list_insert(&layered->monitor->layers[layered->surface->current.layer], &layered->link);
    wlr_surface_send_enter(layered->surface->surface, layered->monitor->output);

    monitor_layers_arrange(layered->monitor);
}

void new_lock(struct wl_listener *listener, void *data) {
    struct wlr_session_lock_v1 *wlr_session_lock = data;
    if (locked) wlr_session_lock_v1_destroy(wlr_session_lock);

    wlr_scene_node_set_enabled(&session_lock.background->node, true);
    wlr_scene_node_set_enabled(&session_lock.surfaces->node, true);

    toplevel_unfocus(sel_mon->sel);

    wlr_cursor_unset_image(cursor);

    locked = true;
    listen(&wlr_session_lock->events.new_surface, &listeners.new_lock_surface, new_lock_surface);
    listen(&wlr_session_lock->events.destroy, &listeners.lock_destroy, session_lock_destroy);
    listen(&wlr_session_lock->events.unlock, &listeners.unlock, unlock);

    wlr_session_lock_v1_send_locked(wlr_session_lock);
}

void new_lock_surface(struct wl_listener *listener, void *data) {
        struct wlr_session_lock_surface_v1 *surface = data;
    struct monitor *monitor = surface->output->data;

    struct wlr_scene_tree *tree = wlr_scene_subsurface_tree_create(session_lock.surfaces, surface->surface);
    monitor->lock_surface = surface;

    wlr_scene_node_set_position(&tree->node, monitor->geometry.x, monitor->geometry.y);
    wlr_session_lock_surface_v1_configure(surface, monitor->geometry.width, monitor->geometry.height);

    listen(&surface->events.destroy, &monitor->listeners.lock_surface_destroy, lock_surface_destroy);

    if (monitor == sel_mon) client_notify_enter(surface->surface, wlr_seat_get_keyboard(seat));
}

void new_output(struct wl_listener *listener, void *data) {
    struct wlr_output *output = data;

    // Small necessary preparations for the output.
    if (!wlr_output_init_render(output, allocator, renderer)) return;

    struct monitor *monitor = ezalloc(sizeof(*monitor));
    monitor->output = output;
    monitor->output->data = monitor;

    struct wlr_output_state state;
    wlr_output_state_init(&state);

    struct wlr_output_mode *mode = wlr_output_preferred_mode(output);
    if (mode) wlr_output_state_set_mode(&state, mode);
    wlr_output_state_set_enabled(&state, true);

    for (const struct monitor_rule *rule = monitor_rules; rule < END(monitor_rules); rule++) {
        if (rule->monitor && !strstr(rule->monitor, monitor->output->name)) continue;
        monitor->geometry.x = rule->x;
        monitor->geometry.y = rule->y;
        wlr_output_state_set_scale(&state, rule->scale);
        wlr_output_state_set_transform(&state, rule->transform);
        break;
    }

    listen(&monitor->output->events.frame, &monitor->listeners.frame, monitor_frame);
    listen(&monitor->output->events.request_state, &monitor->listeners.request_state, monitor_request_state);
    listen(&monitor->output->events.destroy, &monitor->listeners.destroy, monitor_destroy);

    bool ok = wlr_output_commit_state(output, &state);
    wlr_output_state_finish(&state);
    if (!ok) {
        free(monitor);
        return;
    }

    monitor->fullscreen_background = wlr_scene_rect_create(layers[FULLSCREEN], 0, 0, GLSL_COLOR(fullscreen_bg_color));
    wlr_scene_node_set_enabled(&monitor->fullscreen_background->node, false);

    monitor->bar.base.type = WINDOW_BAR;
    assert(swapchain_init(&monitor->bar.swapchain, buffer_allocator_create(), &argb32));
    assert((monitor->bar.scene = wlr_scene_buffer_create(layers[BOTTOM], NULL)));
    monitor->bar.scene->node.data = &monitor->bar.base;
    listen(&monitor->bar.scene->events.frame_done, &monitor->listeners.frame_done, draw_bar);

    wl_list_init(&monitor->toplevels);
    wl_list_init(&monitor->stack);
    for (int i = 0; i < WLR_LAYER_SHELL_LAYERS; i++) wl_list_init(&monitor->layers[i]);

    monitor->tagset[0] = monitor->tagset[1] = 1;
    monitor->layouts[0] = &layouts[0];
    monitor->layouts[1] = &layouts[1 % length(layouts)];
    monitor->master_amount = master_amount;
    monitor->master_factor = master_factor;
    strncpy(monitor->lt_symbol, monitor->layouts[monitor->layout]->symbol, LAYOUT_SYMBOL_MAX);
    wlr_scene_node_set_enabled(&monitor->bar.scene->node, show_bar);

    wl_list_insert(&monitors, &monitor->link);

    assert((monitor->output_scene = wlr_scene_output_create(scene, monitor->output)));
    if (monitor->geometry.x < 0 && monitor->geometry.y < 0) {
        wlr_output_layout_add_auto(output_layout, monitor->output);
    }
    else {
        wlr_output_layout_add(output_layout, monitor->output, monitor->geometry.x, monitor->geometry.y);
    }
}

void new_pointer_constraint(struct wl_listener *listener, void *data) {
    struct pointer_constraint *constraint = ezalloc(sizeof(*constraint));
    constraint->contraint = data;
    constraint->contraint->data = constraint;
    listen(&constraint->contraint->events.destroy, &constraint->listeners.destroy, pointer_constraint_destroy);

    if (constraint->contraint->surface == seat->pointer_state.focused_surface) pointer_constrain(constraint);
}

void new_shortcut_inhibitor(struct wl_listener *listener, void *data) {
    struct wlr_keyboard_shortcuts_inhibitor_v1 *inhibitor = data;
    if (!shortcuts_inhibited) {
        listen(&inhibitor->events.destroy, &listeners.shortcut_inhibitor_destroy, shortcut_inhibitor_destroy);
        wlr_keyboard_shortcuts_inhibitor_v1_activate(inhibitor);
        shortcuts_inhibited = true;
        return;
    }

    wlr_keyboard_shortcuts_inhibitor_v1_deactivate(inhibitor);
}

void new_virtual_keyboard(struct wl_listener *listener, void *data) {
    struct wlr_virtual_keyboard_v1 *keyboard = data;
    wlr_keyboard_set_keymap(&keyboard->keyboard, virtual_keyboards.group->keyboard.keymap);
    wlr_keyboard_set_repeat_info(&keyboard->keyboard, keyboard_repeat_rate, keyboard_repeat_delay);
    wlr_keyboard_group_add_keyboard(virtual_keyboards.group, &keyboard->keyboard);
}

void new_virtual_pointer(struct wl_listener *listener, void *data) {
    struct wlr_virtual_pointer_v1_new_pointer_event *event = data;
    struct wlr_pointer pointer = event->new_pointer->pointer;

    // TODO: Should we use pointer_init with this? Would this count like that?
    // I mean it is sending cursor events. Overall I don't really know how this works.
    wlr_cursor_attach_input_device(cursor, &pointer.base);
    if (event->suggested_output) wlr_cursor_map_input_to_output(cursor, &pointer.base, event->suggested_output);
}

void new_xdg_client(struct wl_listener *listener, void *data) {
    struct wlr_xdg_surface *surface = data;
    struct toplevel *toplevel;
    struct layered *layered;

    assert(surface->role != WLR_XDG_SURFACE_ROLE_NONE);
    if (surface->role == WLR_XDG_SURFACE_ROLE_POPUP) {
        if (!client_from_surface(surface->surface, NULL, &layered, &toplevel)) {
            return;
        }
        surface->surface->data = wlr_scene_xdg_surface_create(layered ? layered->popups : toplevel->scene, surface);

        // TODO: Unconstrain! After layer clients
        // We must unconstrain setting the box to the xy of the parent client
        // and the width / height as either the monitor_space for layer client
        // or toplevel_space for toplevel client.
        if ((layered && !layered->monitor) || (toplevel && !toplevel->monitor)) return;

        struct wlr_box box;
        box = layered ? layered->monitor->geometry : toplevel->monitor->toplevel_space;
        box.x -= (layered ? layered->geometry.x : toplevel->geometry.x);
        box.y -= (layered ? layered->geometry.y : toplevel->geometry.y);
        wlr_xdg_popup_unconstrain_from_box(surface->popup, &box);
        return;
    }

    // WLR_XDG_SURFACE_ROLE_TOPLEVEL
    toplevel = ezalloc(sizeof(*toplevel));
    toplevel->base.type = WINDOW_TOPLEVEL;
    toplevel->type = TOPLEVEL_XDG;
    toplevel->xdg.surface = surface;
    toplevel->xdg.surface->data = toplevel;
    toplevel->border_width = border_width;
    toplevel->scene = wlr_scene_tree_create(layers[TILED]);
    toplevel->scene->node.data = &toplevel->base;
    toplevel->surface = wlr_scene_xdg_surface_create(toplevel->scene, toplevel->xdg.surface);
    for (int i = 0; i < 4; i++) {
        assert((toplevel->border[i] = wlr_scene_rect_create(toplevel->scene, 0, 0, GLSL_COLOR(colors[SCHEME_NORMAL][COLOR_BORDER]))));
    }
    wlr_scene_node_set_enabled(&toplevel->scene->node, false);

    wlr_xdg_toplevel_set_wm_capabilities(toplevel->xdg.surface->toplevel, WLR_XDG_TOPLEVEL_WM_CAPABILITIES_FULLSCREEN);

    listen(&toplevel->xdg.surface->events.destroy, &toplevel->listeners.destroy, toplevel_destroy);
    listen(&toplevel->xdg.surface->toplevel->events.request_fullscreen, &toplevel->listeners.fullscreen, toplevel_fullscreen);
    listen(&toplevel->xdg.surface->toplevel->events.request_maximize, &toplevel->xdg.listeners.maximize, toplevel_maximize);
    listen(&toplevel->xdg.surface->surface->events.commit, &toplevel->xdg.listeners.commit, toplevel_commit);
    listen(&toplevel->xdg.surface->surface->events.map, &toplevel->listeners.map, toplevel_map);
    listen(&toplevel->xdg.surface->surface->events.unmap, &toplevel->listeners.unmap, toplevel_unmap);
}

void new_xwayland_client(struct wl_listener *listener, void *data) {
    struct wlr_xwayland_surface *surface = data;

    struct toplevel *toplevel = ezalloc(sizeof(*toplevel));
    toplevel->base.type = WINDOW_TOPLEVEL;
    toplevel->type = TOPLEVEL_X11;
    toplevel->x11.surface = surface;
    toplevel->x11.surface->data = toplevel;
    toplevel->border_width = border_width;
    toplevel->scene = wlr_scene_tree_create(layers[TILED]);
    toplevel->scene->node.data = &toplevel->base;
    for (int i = 0; i < 4; i++) {
        assert((toplevel->border[i] = wlr_scene_rect_create(toplevel->scene, 0, 0, GLSL_COLOR(colors[SCHEME_NORMAL][COLOR_BORDER]))));
    }

    wlr_scene_node_set_enabled(&toplevel->scene->node, false);

    listen(&surface->events.request_activate, &toplevel->x11.listeners.activate, toplevel_activate);
    listen(&surface->events.associate, &toplevel->x11.listeners.associate, toplevel_associate);
    listen(&surface->events.request_configure, &toplevel->x11.listeners.configure, toplevel_configure);
    listen(&surface->events.destroy, &toplevel->listeners.destroy, toplevel_destroy);
    listen(&surface->events.dissociate, &toplevel->x11.listeners.dissociate, toplevel_dissociate);
    listen(&surface->events.request_fullscreen, &toplevel->listeners.fullscreen, toplevel_fullscreen);
    listen(&surface->events.set_hints, &toplevel->x11.listeners.hints, toplevel_hints);
}

void output_layout_change(struct wl_listener *listener, void *data) {
    wlr_output_layout_get_box(output_layout, NULL, &scene_geometry);

    wlr_scene_node_set_position(&root_background->node, scene_geometry.x, scene_geometry.y);
    wlr_scene_rect_set_size(root_background, scene_geometry.width, scene_geometry.height);

    wlr_scene_node_set_position(&session_lock.background->node, scene_geometry.x, scene_geometry.y);
    wlr_scene_rect_set_size(session_lock.background, scene_geometry.width, scene_geometry.height);

    struct wlr_output_configuration_v1 *config = wlr_output_configuration_v1_create();
    struct monitor *monitor;
    wl_list_for_each(monitor, &monitors, link) {
        if (!sel_mon && monitor->output->enabled) {
            sel_mon = monitor;
            if (wl_list_empty(&monitor->toplevels)) {
                struct monitor *tmp;
                wl_list_for_each(tmp, &monitors, link) {
                    if (tmp == monitor || wl_list_empty(&monitors)) continue;
                    wl_list_insert_list(&monitor->toplevels, &tmp->toplevels);
                    wl_list_insert_list(&monitor->stack, &tmp->stack);
                    wl_list_init(&tmp->toplevels);
                    wl_list_init(&tmp->stack);
                    break;
                }
            }
        }

        monitor_update_geometry(monitor);
        monitor_update_config(monitor, config);
    }

    wlr_output_manager_v1_set_configuration(globals.output_manager, config);
}

void output_manager_apply(struct wl_listener *listener, void *data) {
    struct wlr_output_configuration_v1 *config = data;
    output_manager_change(config, false);
}

void output_manager_change(struct wlr_output_configuration_v1 *config, bool test) {
    if (!config) return;

    // TODO: We need to update our monitors with new state as we go along here
    // then we also need to update the output manager configuration

    bool ok = true, layout_changed = false, okay;
    struct monitor *monitor;
    struct wlr_output_configuration_head_v1 *head;
    wl_list_for_each(head, &config->heads, link) {
        struct wlr_output *output = head->state.output;
        monitor = output->data;

        struct wlr_output_state state;
        wlr_output_state_init(&state);
        wlr_output_head_v1_state_apply(&head->state, &state);

        ok &= okay = test ? wlr_output_test_state(output, &state) : wlr_output_commit_state(output, &state);
        wlr_output_state_finish(&state);

        if (test || !okay) continue;

        wlr_xcursor_manager_load(xcursor, output->scale);

        if (!output->enabled && wlr_output_layout_get(output_layout, output)) {
            wlr_output_layout_remove(output_layout, output);
            monitor_cleanup(monitor);
            layout_changed = true;
            continue;
        }

        if (output->enabled && !wlr_output_layout_get(output_layout, output)) {
            wlr_output_layout_add_auto(output_layout, output);
            layout_changed = true;
            continue;
        }

        if (monitor->geometry.x != head->state.x || monitor->geometry.y != head->state.y) {
            wlr_output_layout_add(output_layout, output, head->state.x, head->state.y);
            layout_changed = true;
        }
    }

    if (ok) {
        wlr_output_configuration_v1_send_succeeded(config);
    }
    else {
        wlr_output_configuration_v1_send_failed(config);
    }

    wlr_output_configuration_v1_destroy(config);

    if (!test && !layout_changed) {
        config = wlr_output_configuration_v1_create();
        wl_list_for_each(monitor, &monitors, link) monitor_update_config(monitor, config);
        wlr_output_manager_v1_set_configuration(globals.output_manager, config);
    }
}

void output_manager_test(struct wl_listener *listener, void *data) {
    struct wlr_output_configuration_v1 *config = data;
    output_manager_change(config, true);
}

void pointer_constrain(struct pointer_constraint *constraint) {
    if (!constraint) {
        active_constraint = NULL;
        return;
    }

    if (active_constraint == constraint->contraint) return;
    if (active_constraint) wlr_pointer_constraint_v1_send_deactivated(active_constraint);

    active_constraint = constraint->contraint;
    wlr_pointer_constraint_v1_send_activated(active_constraint);
}

void pointer_constraint_destroy(struct wl_listener *listener, void *data) {
    struct pointer_constraint *constraint = wl_container_of(listener, constraint, listeners.destroy);

    if (active_constraint == constraint->contraint) {
        active_constraint->data = NULL;
        active_constraint = NULL;
        struct toplevel *toplevel = NULL;
        double x = constraint->contraint->current.cursor_hint.x,
               y = constraint->contraint->current.cursor_hint.y;
        /* TODO: wlroots 0.18: https://gitlab.freedesktop.org/wlroots/wlroots/-/merge_requests/4478 */
        if (client_from_surface(constraint->contraint->surface, NULL, NULL, &toplevel) &&
                toplevel &&
                (constraint->contraint->current.committed & WLR_POINTER_CONSTRAINT_V1_STATE_CURSOR_HINT)) {
            wlr_cursor_warp(cursor, NULL,
                    x + toplevel->geometry.x + toplevel->border_width,
                    y + toplevel->geometry.y + toplevel->border_width);
            wlr_seat_pointer_warp(seat, x, y);
        }
    }

    wl_list_remove(&constraint->listeners.destroy.link);
    free(constraint);
}

void pointer_destroy(struct wl_listener *listener, void *data) {
    struct pointer *pointer = wl_container_of(listener, pointer, listeners.destroy);
    wl_list_remove(&pointer->listeners.destroy.link);
    wl_list_remove(&pointer->link);
    if (wl_list_empty(&pointers)) {
        wlr_cursor_unset_image(cursor);
        seat_update_capabilities();
    }
    free(pointer);
}

void pointer_init(struct wlr_pointer *wlr_pointer) {
    // This is ripped straight from dwl createpointer
    struct libinput_device *device;
    if (wlr_input_device_is_libinput(&wlr_pointer->base) && (device = wlr_libinput_get_device_handle(&wlr_pointer->base))) {
		if (libinput_device_config_tap_get_finger_count(device)) {
			libinput_device_config_tap_set_enabled(device, tap_to_click);
			libinput_device_config_tap_set_drag_enabled(device, tap_and_drag);
			libinput_device_config_tap_set_drag_lock_enabled(device, drag_lock);
			libinput_device_config_tap_set_button_map(device, button_map);
		}

		if (libinput_device_config_scroll_has_natural_scroll(device))
			libinput_device_config_scroll_set_natural_scroll_enabled(device, natural_scrolling);

		if (libinput_device_config_dwt_is_available(device))
			libinput_device_config_dwt_set_enabled(device, disable_while_typing);

		if (libinput_device_config_left_handed_is_available(device))
			libinput_device_config_left_handed_set(device, left_handed);

		if (libinput_device_config_middle_emulation_is_available(device))
			libinput_device_config_middle_emulation_set_enabled(device, middle_button_emulation);

		if (libinput_device_config_scroll_get_methods(device) != LIBINPUT_CONFIG_SCROLL_NO_SCROLL)
			libinput_device_config_scroll_set_method (device, scroll_method);

		if (libinput_device_config_click_get_methods(device) != LIBINPUT_CONFIG_CLICK_METHOD_NONE)
			libinput_device_config_click_set_method (device, click_method);

		if (libinput_device_config_send_events_get_modes(device))
			libinput_device_config_send_events_set_mode(device, send_events_mode);

		if (libinput_device_config_accel_is_available(device)) {
			libinput_device_config_accel_set_profile(device, accel_profile);
			libinput_device_config_accel_set_speed(device, accel_speed);
		}
    }

    // hopefully this allows us to have multiple pointers.
    wlr_cursor_attach_input_device(cursor, &wlr_pointer->base);

    struct pointer *pointer = ezalloc(sizeof(*pointer));
    pointer->pointer = wlr_pointer;
    if (wl_list_empty(&pointers)) wlr_cursor_set_xcursor(cursor, xcursor, "default");
    wl_list_insert(&pointers, &pointer->link);
    listen(&wlr_pointer->base.events.destroy, &pointer->listeners.destroy, pointer_destroy);
}

void quit(const union arg *arg) {
    wl_display_terminate(display);
}

void run(void) {
    assert(wlr_backend_start(backend));

    sel_mon = !wl_list_empty(&monitors) ? wl_container_of(monitors.next, sel_mon, link) : NULL;

    wl_display_run(display);
}

void seat_set_cursor(struct wl_listener *listener, void *data) {
    struct wlr_seat_pointer_request_set_cursor_event *event = data;
    if (seat->pointer_state.focused_client == event->seat_client) wlr_cursor_set_surface(cursor, event->surface, event->hotspot_x, event->hotspot_y);
}

void seat_set_primary_selection(struct wl_listener *listener, void *data) {
    struct wlr_seat_request_set_primary_selection_event *event = data;
    wlr_seat_set_primary_selection(seat, event->source, event->serial);
}

void seat_set_request_start_drag(struct wl_listener *listener, void *data) {
    struct wlr_seat_request_start_drag_event *event = data;

    if (wlr_seat_validate_pointer_grab_serial(seat, event->origin, event->serial)) {
        wlr_seat_start_pointer_drag(seat, event->drag, event->serial);
    }
    else {
        wlr_data_source_destroy(event->drag->source);
    }
}

void seat_set_selection(struct wl_listener *listener, void *data) {
    struct wlr_seat_request_set_selection_event *event = data;
    wlr_seat_set_selection(seat, event->source, event->serial);
}

void seat_set_start_drag(struct wl_listener *listener, void *data) {
    struct wlr_drag *drag = data;
    if (!drag->icon) return;

    // NOTE: Depending on what happens in the backend this could be a bad idea.
    drag->icon->data = &wlr_scene_drag_icon_create(drag_icon, drag->icon)->node;
    listen(&drag->icon->events.destroy, &listeners.drag_icon_destroy, drag_icon_destroy);
}

void seat_update_capabilities(void) {
    uint32_t capabilites = 0;
    if (!wl_list_empty(&pointers))  capabilites |= WL_SEAT_CAPABILITY_POINTER;
    if (!wl_list_empty(&keyboards.group->devices) || !wl_list_empty(&virtual_keyboards.group->devices)) capabilites |= WL_SEAT_CAPABILITY_KEYBOARD;
    if (capabilites != seat->capabilities) wlr_seat_set_capabilities(seat, capabilites);
}

void session_lock_destroy(struct wl_listener *listener, void *data) {
    lock_destroy();
}

void setup(void) {
    uint32_t flags = fcntl(STDIN_FILENO, F_GETFD);
    assert(fcntl(STDIN_FILENO, F_SETFD, flags | O_NONBLOCK) != -1);

    int sig[] = {SIGCHLD, SIGPIPE, SIGINT, SIGTERM};
    struct sigaction sa = {.sa_flags = SA_RESTART, .sa_handler = signal_handle};
    for (int i = 0; i < length(sig); i++) sigaction(sig[i], &sa, NULL);

    wl_list_init(&pointers);
    wl_list_init(&monitors);

    assert(fcft_init(FCFT_LOG_COLORIZE_AUTO, false, FCFT_LOG_CLASS_ERROR));
    assert(fcft_set_scaling_filter(FCFT_SCALING_FILTER_LANCZOS3));
    assert(font = fcft_from_name(length(fonts), fonts, NULL));

    bar_height = font->height + 2;

    assert(display = wl_display_create());
    assert(backend = wlr_backend_autocreate(display, &session));
    listen(&backend->events.new_output, &listeners.new_output, new_output);
    listen(&backend->events.new_input, &listeners.new_input, new_input);

    assert(renderer = wlr_renderer_autocreate(backend));
    assert(allocator = wlr_allocator_autocreate(backend, renderer));

    assert(scene = wlr_scene_create());
    assert(root_background = wlr_scene_rect_create(&scene->tree, 0, 0, GLSL_COLOR(root_color)))
    for (int i = 0; i < NUM_LAYERS; i++) assert(layers[i] = wlr_scene_tree_create(&scene->tree));

    assert(drag_icon = wlr_scene_tree_create(&scene->tree));
    wlr_scene_node_place_below(&drag_icon->node, &layers[LOCK]->node);

	/* Create shm, drm and linux_dmabuf interfaces by ourselves.
	 * The simplest way is call:
	 *      wlr_renderer_init_wl_display(drw);
	 * but we need to create manually the linux_dmabuf interface to integrate it
	 * with wlr_scene. */
	assert(wlr_renderer_init_wl_shm(renderer, display));
	if (wlr_renderer_get_dmabuf_texture_formats(renderer)) {
        wlr_drm_create(display, renderer);
        struct wlr_linux_dmabuf_v1 *linux_dmabuf = wlr_linux_dmabuf_v1_create_with_renderer(display, 4, renderer);
        assert(linux_dmabuf);
		wlr_scene_set_linux_dmabuf_v1(scene, linux_dmabuf);
	}

    assert(output_layout = wlr_output_layout_create());
    listen(&output_layout->events.change, &listeners.output_layout_change, output_layout_change);

    assert(wlr_subcompositor_create(display));
    assert(wlr_data_device_manager_create(display));
    assert(wlr_export_dmabuf_manager_v1_create(display));
    assert(wlr_screencopy_manager_v1_create(display));
    assert(wlr_data_control_manager_v1_create(display));
    assert(wlr_primary_selection_v1_device_manager_create(display));
    assert(wlr_viewporter_create(display));
    assert(wlr_single_pixel_buffer_manager_v1_create(display));
    assert(wlr_fractional_scale_manager_v1_create(display, 1));
    assert(wlr_xdg_output_manager_v1_create(display, output_layout));
    assert(globals.compositor = wlr_compositor_create(display, 6, renderer));
    assert(globals.idle_notifier = wlr_idle_notifier_v1_create(display));

    assert(globals.output_manager = wlr_output_manager_v1_create(display));
    listen(&globals.output_manager->events.apply, &listeners.output_manager_apply, output_manager_apply);
    listen(&globals.output_manager->events.test, &listeners.output_manager_test, output_manager_test);

    struct wlr_presentation *presentation = wlr_presentation_create(display, backend);
    assert(presentation);
    wlr_scene_set_presentation(scene, presentation);

    struct wlr_xdg_shell *xdg_shell = wlr_xdg_shell_create(display, 6);
    assert(xdg_shell);
    listen(&xdg_shell->events.new_surface, &listeners.new_xdg_client, new_xdg_client);

    struct wlr_layer_shell_v1 *layer_shell = wlr_layer_shell_v1_create(display, 4);
    assert(layer_shell);
    listen(&layer_shell->events.new_surface, &listeners.new_layer_client, new_layer_client);

    if ((xwayland = wlr_xwayland_create(display, globals.compositor, true))) {
        listen(&xwayland->events.ready, &listeners.xwayland_ready, xwayland_ready);
        listen(&xwayland->events.new_surface, &listeners.new_xwayland_client, new_xwayland_client);
        setenv("DISPLAY", xwayland->server->display_name, 1);
    }

    struct wlr_session_lock_manager_v1 *lock_manager = wlr_session_lock_manager_v1_create(display);
    // NOTE: This will be initialized later when the monitors are updated.
    session_lock.background = wlr_scene_rect_create(layers[LOCK], 0, 0, GLSL_COLOR(locked_color));
    session_lock.surfaces = wlr_scene_tree_create(layers[LOCK]);

    assert(lock_manager && session_lock.background && session_lock.surfaces);

    wlr_scene_node_set_enabled(&session_lock.background->node, false);
    wlr_scene_node_set_enabled(&session_lock.surfaces->node, false);
    listen(&lock_manager->events.new_lock, &listeners.new_lock, new_lock);
    listen(&lock_manager->events.destroy, &listeners.lock_manager_destroy, lock_manager_destroy);

    struct wlr_server_decoration_manager *server_decoration_manager = wlr_server_decoration_manager_create(display);
    assert(server_decoration_manager);
    wlr_server_decoration_manager_set_default_mode(server_decoration_manager, WLR_SERVER_DECORATION_MANAGER_MODE_SERVER);

    struct wlr_xdg_decoration_manager_v1 *decoration_manager = wlr_xdg_decoration_manager_v1_create(display);
    assert(decoration_manager);
    listen(&decoration_manager->events.new_toplevel_decoration, &listeners.new_decoration, new_decoration);

    assert(globals.idle_inhibit = wlr_idle_inhibit_v1_create(display));
    listen(&globals.idle_inhibit->events.new_inhibitor, &listeners.new_idle_inhibitor, new_idle_inhibitor);

    assert(globals.gamma_control = wlr_gamma_control_manager_v1_create(display));
    listen(&globals.gamma_control->events.set_gamma, &listeners.gamma_changed, gamma_changed);

    struct wlr_xdg_activation_v1 *activation = wlr_xdg_activation_v1_create(display);
    assert(activation);
    listen(&activation->events.request_activate, &listeners.xdg_activation_activate, xdg_activation_activate);

    assert(keyboards.group = wlr_keyboard_group_create());
    assert(virtual_keyboards.group = wlr_keyboard_group_create());

    struct xkb_context *context = xkb_context_new(XKB_CONTEXT_NO_FLAGS);
    assert(context);
    struct xkb_keymap *keymap = xkb_keymap_new_from_names(context, NULL, XKB_KEYMAP_COMPILE_NO_FLAGS);
    assert(keymap);

    assert(wlr_keyboard_set_keymap(&keyboards.group->keyboard, keymap));
    assert(wlr_keyboard_set_keymap(&virtual_keyboards.group->keyboard, keymap));
    xkb_keymap_unref(keymap);
    xkb_context_unref(context);

    wlr_keyboard_set_repeat_info(&keyboards.group->keyboard, keyboard_repeat_rate, keyboard_repeat_delay);
    wlr_keyboard_set_repeat_info(&virtual_keyboards.group->keyboard, keyboard_repeat_rate, keyboard_repeat_delay);

    keyboards.key_repeat = wl_event_loop_add_timer(wl_display_get_event_loop(display), keyboard_repeat, &keyboards);
    virtual_keyboards.key_repeat = wl_event_loop_add_timer(wl_display_get_event_loop(display), keyboard_repeat, &virtual_keyboards);

    wl_list_init(&keyboards.keyboards);
    wl_list_init(&virtual_keyboards.keyboards);

    listen(&keyboards.group->keyboard.events.key, &keyboards.listeners.key, keyboard_key);
    listen(&keyboards.group->keyboard.events.modifiers, &keyboards.listeners.modifiers, keyboard_modifiers);
    listen(&virtual_keyboards.group->keyboard.events.key, &virtual_keyboards.listeners.key, keyboard_key);
    listen(&virtual_keyboards.group->keyboard.events.modifiers, &virtual_keyboards.listeners.modifiers, keyboard_modifiers);

    assert(seat = wlr_seat_create(display, "seat0"));
    listen(&seat->events.request_set_cursor, &listeners.seat_set_cursor, seat_set_cursor);
    listen(&seat->events.request_set_selection, &listeners.seat_set_selection, seat_set_selection);
    listen(&seat->events.request_set_primary_selection, &listeners.seat_set_primary_selection, seat_set_primary_selection);
    listen(&seat->events.request_start_drag, &listeners.seat_set_request_start_drag, seat_set_request_start_drag);
    listen(&seat->events.start_drag, &listeners.seat_set_start_drag, seat_set_start_drag);

    assert(globals.cursor_shape = wlr_cursor_shape_manager_v1_create(display, 1));
    listen(&globals.cursor_shape->events.request_set_shape, &listeners.cursor_shape_set_request, cursor_shape_set_request);

    assert(cursor = wlr_cursor_create());
    wlr_cursor_attach_output_layout(cursor, output_layout);
    wlr_cursor_unset_image(cursor); /* Until we get a pointer later */
    listen(&cursor->events.motion, &listeners.cursor_motion, cursor_motion);
    listen(&cursor->events.motion_absolute, &listeners.cursor_motion_absolute, cursor_motion_absolute);
    listen(&cursor->events.button, &listeners.cursor_button, cursor_button);
    listen(&cursor->events.axis, &listeners.cursor_axis, cursor_axis);
    listen(&cursor->events.frame, &listeners.cursor_frame, cursor_frame);

    assert(xcursor = wlr_xcursor_manager_create(NULL, 24));
	setenv("XCURSOR_SIZE", "24", 1);

    struct wlr_virtual_keyboard_manager_v1 *virt_keyboard_manager = wlr_virtual_keyboard_manager_v1_create(display);
    assert(virt_keyboard_manager);
    listen(&virt_keyboard_manager->events.new_virtual_keyboard, &listeners.new_virtual_keyboard, new_virtual_keyboard);

    struct wlr_virtual_pointer_manager_v1 *virt_pointer_manager = wlr_virtual_pointer_manager_v1_create(display);
    assert(virt_pointer_manager);
    listen(&virt_pointer_manager->events.new_virtual_pointer, &listeners.new_virtual_pointer, new_virtual_pointer);

    assert(globals.relative_manager = wlr_relative_pointer_manager_v1_create(display));
    assert(globals.pointer_constraints = wlr_pointer_constraints_v1_create(display));
    listen(&globals.pointer_constraints->events.new_constraint, &listeners.new_pointer_constraint, new_pointer_constraint);

    struct wlr_keyboard_shortcuts_inhibit_manager_v1 *shortcuts_manager = wlr_keyboard_shortcuts_inhibit_v1_create(display);
    assert(shortcuts_manager);
    listen(&shortcuts_manager->events.new_inhibitor, &listeners.new_shortcut_inhibitor, new_shortcut_inhibitor);

    const char *socket = wl_display_add_socket_auto(display);
    assert(socket);
    setenv("WAYLAND_DISPLAY", socket, 1);

    stdin_source = wl_event_loop_add_fd(wl_display_get_event_loop(display), STDIN_FILENO, WL_EVENT_READABLE, stdin_in, NULL);

    setenv("XDG_CURRENT_DESKTOP", "wlroots", 1);
}

void set_layout(const union arg *arg) {
    if (!sel_mon) return;
    if (!arg || !arg->v || arg->v != sel_mon->layouts[sel_mon->layout]) sel_mon->layout ^= 1;
    if (arg->v) sel_mon->layouts[sel_mon->layout] = (struct layout*)arg->v;
    strncpy(sel_mon->lt_symbol, sel_mon->layouts[sel_mon->layout]->symbol, LAYOUT_SYMBOL_MAX);
    if (sel_mon->sel) monitor_arrange(sel_mon);
}

void shortcut_inhibitor_destroy(struct wl_listener *listener, void *data) {
    wl_list_remove(&listener->link);
    shortcuts_inhibited = false;
}

void signal_handle(int signal) {
    if (signal == SIGCHLD) {
        siginfo_t in;
		while (!waitid(P_ALL, 0, &in, WEXITED|WNOHANG|WNOWAIT) &&
                in.si_pid &&
                (!xwayland || in.si_pid != xwayland->server->pid)) {
            waitpid(in.si_pid, NULL, 0);
        }
        return;
    }

    if (signal == SIGINT || signal == SIGTERM) {
        quit(NULL);
    }

    // we don't handle SIGPIPE
}

void spawn(const union arg *arg) {
    if (fork() == 0) {
		setsid();
		execvp(((char **)arg->v)[0], (char **)arg->v);
        panic("execvp failed: %s", ((char **)arg->v)[0]);
    }
}

int32_t stdin_in(int32_t fd, uint32_t mask, void *data) {
    if (mask & (WL_EVENT_ERROR | WL_EVENT_HANGUP)) return 0;

    fd = dup(fd);
    if (fd == -1) return 0;

    FILE *file = fdopen(fd, "r");
    if (!file) return 0;

    // NOTE: This might not be as portable as we want.
    // So we might want to change this.
    char *str = NULL;
    size_t len;
    ssize_t out = getline(&str, &len, file);
    if (out == -1) {
        free(str);
        return 0;
    }

    uint32_t status_len = strlen("status ");
    if (STRINGN_EQUAL(str, "status ", status_len)){
        char *status_str = strtok(str+status_len, "\n");
        if (!status_str) status_str = "";
        status_len = strlen(status_str);

        free(status);
        status = status_len ? strdup(status_str) : NULL;
    }

    fclose(file);
    free(str);
    return 0;
}

void tag(const union arg *arg) {
    if (!sel_mon->sel || !(arg->ui & TAGMASK)) return;

    sel_mon->sel->tags = arg->ui & TAGMASK;
    toplevel_focus(monitor_get_top(sel_mon), true);
    monitor_arrange(sel_mon);
}

uint32_t text_draw(struct fcft_font *font, pixman_image_t *image, const char *text, const pixman_color_t *text_color, const struct wlr_box *box) {
    if (!text) return 0;

    int64_t kern;
    uint32_t state = UTF8_ACCEPT, previous_codepoint = 0, codepoint, x = box->x == -1 ? 0 : box->x, n = x;
    pixman_image_t *text_image = image && text_color ? pixman_image_create_solid_fill(text_color) : NULL;
    for (; *text; text++) {
        if (utf8decode(&state, &codepoint, *text)) continue;

        const struct fcft_glyph *glyph = fcft_rasterize_char_utf32(font, codepoint, FCFT_SUBPIXEL_NONE);
        if (!glyph) continue;

        kern = 0;
        if (previous_codepoint) fcft_kerning(font, previous_codepoint, codepoint, &kern, NULL);

        if (box->width != -1 && x + kern + glyph->advance.x > n + box->width) break;

        if (text_image) {
            if (pixman_image_get_format(glyph->pix) == PIXMAN_a8r8g8b8) {
                pixman_image_composite32(PIXMAN_OP_OVER, glyph->pix, text_image, image,
                        0, 0, 0, 0,
                        x + kern + glyph->x, box->y - glyph->y,
                        glyph->width, glyph->height);
            } else {
                pixman_image_composite32(PIXMAN_OP_OVER, text_image, glyph->pix, image,
                        0, 0, 0, 0,
                        x + kern + glyph->x, box->y - glyph->y,
                        glyph->width, glyph->height);
            }
        }

        x += kern + glyph->advance.x;
        previous_codepoint = codepoint;
    }

    if (text_image) pixman_image_unref(text_image);

    return x;
}

uint32_t text_width(struct fcft_font *font, const char *text) {
    const struct wlr_box box = {.x = 0, .y = 0, .width = -1, .height = -1};
    return text_draw(font, NULL, text, NULL, &box);
}

void tile(struct monitor *monitor) {
    uint32_t n = 0, i, master_width, master_y, stack_y;

    struct toplevel *toplevel;
    wl_list_for_each(toplevel, &monitor->toplevels, link) {
        if (CLIENT_VISIBLE_ON(toplevel, monitor) && !toplevel->floating && !toplevel->fullscreen) {
            n++;
        }
    }
    if (!n) return;

    if (n > monitor->master_amount) {
        master_width = monitor->master_amount ? monitor->toplevel_space.width * monitor->master_factor : 0;
    } else {
        master_width = monitor->toplevel_space.width;
    }

    i = master_y = stack_y = 0;
    wl_list_for_each(toplevel, &monitor->toplevels, link) {
        if (!CLIENT_VISIBLE_ON(toplevel, monitor) || toplevel->floating || toplevel->fullscreen) continue;

        if (i < monitor->master_amount) {
            toplevel_resize(toplevel, &(struct wlr_box){
                    .x = monitor->toplevel_space.x,
                    .y = monitor->toplevel_space.y + master_y,
                    .width = master_width,
                    .height = (monitor->toplevel_space.height - master_y) / (MIN(n, monitor->master_amount) - i),
                    },
                    &monitor->toplevel_space);
            master_y += toplevel->geometry.height;
        }
        else {
            toplevel_resize(toplevel, &(struct wlr_box){
                    .x = monitor->toplevel_space.x + master_width,
                    .y = monitor->toplevel_space.y + stack_y,
                    .width = monitor->toplevel_space.width - master_width,
                    .height = (monitor->toplevel_space.height - stack_y) / (n - i),
                    },
                    &monitor->toplevel_space);
            stack_y += toplevel->geometry.height;
        }

        i++;
    }
}

void toggle_bar(const union arg *arg) {
    if (!sel_mon) return;
    wlr_scene_node_set_enabled(&sel_mon->bar.scene->node, !sel_mon->bar.scene->node.enabled);
    monitor_update_geometry(sel_mon);
}

void toggle_floating(const union arg *arg) {
    if (!sel_mon || !sel_mon->sel) return;
    struct toplevel *toplevel = sel_mon->sel;
    toplevel_set_floating(toplevel, (!toplevel->floating || toplevel_wants_floating(toplevel)));
}

void toggle_fullscreen(const union arg *arg) {
    if (!sel_mon || !sel_mon->sel) return;
    struct toplevel *toplevel = sel_mon->sel;
    toplevel_set_fullscreen(toplevel, !toplevel->fullscreen);
}

void toggle_tag(const union arg *arg) {
    if (!sel_mon->sel) return;

    uint32_t new_tags = sel_mon->sel->tags ^ (arg->ui & TAGMASK);
    if (!new_tags) return;

    sel_mon->sel->tags = new_tags;
    toplevel_focus(monitor_get_top(sel_mon), true);
    monitor_arrange(sel_mon);
}

void toggle_view(const union arg *arg) {
    struct monitor *monitor = sel_mon;
    uint32_t new_viewset;
    if (!monitor || !(new_viewset = monitor->tagset[monitor->sel_tags] ^ (arg->ui & TAGMASK))) return;
    monitor->tagset[monitor->sel_tags] = new_viewset;

    toplevel_focus(monitor_get_top(sel_mon), false);
    monitor_arrange(monitor);
}

void toplevel_activate(struct wl_listener *listener, void *data) {
    struct toplevel *toplevel = wl_container_of(listener, toplevel, x11.listeners.activate);
    if (toplevel_is_unmanaged(toplevel)) wlr_xwayland_surface_activate(toplevel->x11.surface, true);
}

void toplevel_apply_rules(struct toplevel *toplevel) {
    if (!toplevel) return;

    const char *title = toplevel_get_title(toplevel);
    if (!title) title = broken;

    const char *appid = toplevel_get_appid(toplevel);
    if (!appid) appid = broken;

    struct monitor *monitor = sel_mon;
    uint32_t tags = monitor->tagset[monitor->sel_tags];
    bool floating = toplevel_wants_floating(toplevel);

    for (int i = 0; i < length(app_rules); i++) {
        const struct app_rule *rule = &app_rules[i];
        if ((rule->title && !strstr(rule->title, title)) ||
                (rule->appid && !strstr(rule->appid, appid))) continue;

        tags = rule->tags ? rule->tags : tags;
        floating = rule->floating || floating;

        struct monitor *pos;
        wl_list_for_each(pos, &monitors, link) {
            if (!rule->monitor || !STRING_EQUAL(rule->monitor, pos->output->name)) continue;
            tags = pos->tagset[pos->sel_tags];
            monitor = pos;
            break;
        }

        break;
    }

    toplevel->floating = floating;
    toplevel_set_monitor(toplevel, monitor, tags);
}

void toplevel_associate(struct wl_listener *listener, void *data) {
    struct toplevel *toplevel = wl_container_of(listener, toplevel, x11.listeners.associate);
    toplevel->surface = wlr_scene_subsurface_tree_create(toplevel->scene, toplevel_get_surface(toplevel));
    listen(&toplevel_get_surface(toplevel)->events.map, &toplevel->listeners.map, toplevel_map);
    listen(&toplevel_get_surface(toplevel)->events.unmap, &toplevel->listeners.unmap, toplevel_unmap);
}

// NOTE: See the comment in monitor_frame about the outstanding resizes and stuff
void toplevel_commit(struct wl_listener *listener, void *data) {
    struct toplevel *toplevel = wl_container_of(listener, toplevel, xdg.listeners.commit);

    // NOTE: For some reason, without this certain clients like firefox
    // have weird geometry problems and corners of the client are left unrendered.
    // Why is this happening?
    if (toplevel_get_surface(toplevel)->mapped && toplevel->monitor) {
        toplevel_resize(toplevel, &toplevel->geometry, (toplevel->floating && !toplevel->fullscreen) ? &scene_geometry : &toplevel->monitor->toplevel_space);
    }

    //if (toplevel->resize_serial && toplevel->resize_serial <= toplevel->xdg.surface->current.configure_serial) {
    //    wlr_log(WLR_DEBUG, "erase serial");
    //    toplevel->resize_serial = 0;
    //}
}

void toplevel_configure(struct wl_listener *listener, void *data) {
    struct toplevel *toplevel = wl_container_of(listener, toplevel, x11.listeners.configure);
    struct wlr_xwayland_surface_configure_event *event = data;
    if (!toplevel->monitor) {
        wlr_xwayland_surface_configure(toplevel->x11.surface,
                event->x, event->y,
                event->width, event->height);
        return;
    }

    if (toplevel->floating || toplevel_is_unmanaged(toplevel)) {
        toplevel_resize(toplevel, &(struct wlr_box){
                event->x, event->y,
                event->width, event->height,
                },
                &toplevel->monitor->toplevel_space);
    }
    else {
        monitor_arrange(toplevel->monitor);
    }
}

void toplevel_destroy(struct wl_listener *listener, void *data){
    struct toplevel *toplevel = wl_container_of(listener, toplevel, listeners.destroy);
    switch (toplevel->type) {
        case TOPLEVEL_XDG:
            // map and unmap are removed by x11 client on dissociate, xdg keeps them always until destruction.
            wl_list_remove(&toplevel->listeners.map.link);
            wl_list_remove(&toplevel->listeners.unmap.link);
            //wl_list_remove(&toplevel->listeners.commit.link);
            wl_list_remove(&toplevel->xdg.listeners.maximize.link);
            break;
        case TOPLEVEL_X11:
            wl_list_remove(&toplevel->x11.listeners.hints.link);
            wl_list_remove(&toplevel->x11.listeners.activate.link);
            wl_list_remove(&toplevel->x11.listeners.associate.link);
            wl_list_remove(&toplevel->x11.listeners.configure.link);
            wl_list_remove(&toplevel->x11.listeners.dissociate.link);
            break;
    }
    wl_list_remove(&toplevel->listeners.fullscreen.link);
    wl_list_remove(&toplevel->listeners.destroy.link);

    wlr_scene_node_destroy(&toplevel->scene->node);

    free(toplevel);
}

void toplevel_dissociate(struct wl_listener *listener, void *data) {
    struct toplevel *toplevel = wl_container_of(listener, toplevel, x11.listeners.dissociate);
    wl_list_remove(&toplevel->listeners.map.link);
    wl_list_remove(&toplevel->listeners.unmap.link);
}

void toplevel_focus(struct toplevel *toplevel, bool raise) {
    if (locked || (toplevel && !toplevel->monitor)) return;

    // TODO: We may want to restore old behavior here in which we use monitor_get_top in the case of a NULL toplevel.
    // But I would prefer that we don't do that and callers do that themselves as it would make catching errors easier.
    // Either way it is useful to pass in a NULL toplevel in the case that we want to unfocus the current toplevel
    // even if there isn't a new toplevel to focus.

    if (sel_mon->sel && sel_mon->sel != toplevel && !toplevel_wants_focus(toplevel)) toplevel_unfocus(sel_mon->sel);
    if (toplevel) {
        if (raise) wlr_scene_node_raise_to_top(&toplevel->scene->node);
        if (!toplevel_is_unmanaged(toplevel)) {
            if (sel_mon != toplevel->monitor) sel_mon = toplevel->monitor;
            wl_list_remove(&toplevel->stack_link);
            wl_list_insert(&toplevel->monitor->stack, &toplevel->stack_link);
            toplevel_restack(toplevel);
            toplevel->urgent = false;

            if (!seat->drag && !exclusive_focus) toplevel_set_border(toplevel, colors[SCHEME_SELECTED][COLOR_BORDER]);
            client_notify_enter(toplevel_get_surface(toplevel), wlr_seat_get_keyboard(seat));
            toplevel_set_activated(toplevel, true);
        }
    }
    sel_mon->sel = toplevel;

    // TODO: Motion notify?

    // TODO: Check and see if we can activate the client by checking if the previous surface is a layered client
}

struct toplevel *toplevel_from_xy(uint32_t x, uint32_t y, double *nx, double *ny) {
    struct window *window = window_from_xy(x, y, nx, ny, NULL);
    return window && window->type == WINDOW_TOPLEVEL ? toplevel_try_from_window(window) : NULL;
}

void toplevel_fullscreen(struct wl_listener *listener, void *data) {
    struct toplevel *toplevel = wl_container_of(listener, toplevel, listeners.fullscreen);
    toplevel_set_fullscreen(toplevel, toplevel_wants_fullscreen(toplevel));
}

void toplevel_hints(struct wl_listener *listener, void *data) {
    struct toplevel *toplevel = wl_container_of(listener, toplevel, x11.listeners.hints);
    if (toplevel == monitor_get_top(toplevel->monitor)) return;

    toplevel->urgent = xcb_icccm_wm_hints_get_urgency(toplevel->x11.surface->hints);

    if (toplevel->urgent && toplevel_get_surface(toplevel)->mapped) toplevel_set_border(toplevel, colors[SCHEME_URGENT][COLOR_BORDER]);
}

bool toplevel_is_rendered(struct toplevel *toplevel, struct monitor *monitor) {
    if (!toplevel || !monitor) return false;

    int32_t unused;
    if (!wlr_scene_node_coords(&toplevel->scene->node, &unused, &unused)) return false;

    struct wlr_surface_output *output;
    wl_list_for_each(output, &toplevel_get_surface(toplevel)->current_outputs, link) {
        // NOTE: This is a bit of a hack because we can't really get a definition
        // for struct monitor into client.c so we should definitely consider either
        // moving all of this to main.c or making this inline into client.h
        if (output->output == monitor->output) return true;
    }

    return false;
}

void toplevel_kill(const union arg *arg) {
    if (!sel_mon || !sel_mon->sel) return;
    toplevel_send_close(sel_mon->sel);
}

void toplevel_map(struct wl_listener *listener, void *data) {
    struct toplevel *toplevel = wl_container_of(listener, toplevel, listeners.map);

    wlr_scene_node_set_enabled(&toplevel->scene->node, toplevel->type != TOPLEVEL_XDG);

    toplevel_get_geometry(toplevel, &toplevel->geometry);

    if (toplevel_is_unmanaged(toplevel)) {
        wlr_scene_node_reparent(&toplevel->scene->node, layers[FLOATING]);
        wlr_scene_node_set_position(&toplevel->scene->node, toplevel->geometry.x + border_width, toplevel->geometry.y + border_width);
        if (toplevel_wants_focus(toplevel)) {
            toplevel_focus(toplevel, true);
            exclusive_focus = &toplevel->base;
        }

        goto unfullscreen;
    }

	toplevel_set_tiled(toplevel, WLR_EDGE_TOP | WLR_EDGE_BOTTOM | WLR_EDGE_LEFT | WLR_EDGE_RIGHT);
    toplevel->geometry.width += (toplevel->border_width * 2);
    toplevel->geometry.height += (toplevel->border_width * 2);

    struct toplevel *parent = NULL;
    if ((parent = toplevel_get_parent(toplevel))) {
        toplevel->floating = true;
        toplevel_set_monitor(toplevel, parent->monitor, parent->tags);
        toplevel_resize(toplevel, &toplevel->geometry, &toplevel->monitor->toplevel_space);
    }
    else {
        toplevel_apply_rules(toplevel);
    }

    struct monitor *monitor;
    struct toplevel *top;
unfullscreen:
    monitor = toplevel->monitor ? toplevel->monitor : monitor_from_xy(toplevel->geometry.x, toplevel->geometry.y);
    wl_list_for_each(top, &monitor->toplevels, link) {
        if (top != toplevel && top->fullscreen && top->tags & toplevel->tags) toplevel_set_fullscreen(toplevel, false);
    }
}

void toplevel_maximize(struct wl_listener *listener, void *data) {
    struct toplevel *toplevel = wl_container_of(listener, toplevel, xdg.listeners.maximize);
	if (wl_resource_get_version(toplevel->xdg.surface->toplevel->resource)
			< XDG_TOPLEVEL_WM_CAPABILITIES_SINCE_VERSION)
		wlr_xdg_surface_schedule_configure(toplevel->xdg.surface);
}

void toplevel_resize(struct toplevel *toplevel, struct wlr_box *geometry, struct wlr_box *bound) {
    if (!toplevel || !geometry) return;
    struct monitor *monitor = toplevel->monitor;

    geometry->width  = MAX(1 + (2 * toplevel->border_width), geometry->width);
    geometry->height = MAX(1 + (2 * toplevel->border_width), geometry->height);
    toplevel_set_bounds(toplevel, geometry);
    if (bound) box_apply_bounds(geometry, bound);
    toplevel->geometry = *geometry;

    wlr_scene_node_set_position(&toplevel->scene->node, toplevel->geometry.x, toplevel->geometry.y);
    wlr_scene_node_set_position(&toplevel->surface->node, toplevel->border_width, toplevel->border_width);
    wlr_scene_node_set_position(&toplevel->border[0]->node, 0, 0);
    wlr_scene_node_set_position(&toplevel->border[1]->node, 0, 0);
    wlr_scene_node_set_position(&toplevel->border[2]->node, 0, toplevel->geometry.height - toplevel->border_width);
    wlr_scene_node_set_position(&toplevel->border[3]->node, toplevel->geometry.width - toplevel->border_width, 0);
    wlr_scene_rect_set_size(toplevel->border[0], toplevel->geometry.width, toplevel->border_width);
    wlr_scene_rect_set_size(toplevel->border[1], toplevel->border_width, toplevel->geometry.height);
    wlr_scene_rect_set_size(toplevel->border[2], toplevel->geometry.width, toplevel->border_width);
    wlr_scene_rect_set_size(toplevel->border[3], toplevel->border_width, toplevel->geometry.height);

    toplevel->resize_serial = toplevel_set_size(toplevel, toplevel->geometry.width - (toplevel->border_width * 2),
            toplevel->geometry.height - (toplevel->border_width * 2));

    struct wlr_box clip;
    toplevel_get_clip(toplevel, &clip);
    wlr_scene_subsurface_tree_set_clip(&toplevel->surface->node, &clip);
}

void toplevel_set_floating(struct toplevel *toplevel, bool floating) {
    if (!toplevel || toplevel->fullscreen) return;

    toplevel->floating = floating;
    wlr_scene_node_reparent(&toplevel->scene->node, layers[floating ? FLOATING : TILED]);
    monitor_arrange(toplevel->monitor);
}

void toplevel_set_fullscreen(struct toplevel *toplevel, bool fullscreen) {
    if (!toplevel || toplevel->fullscreen == fullscreen || !toplevel->monitor) return;
    toplevel->fullscreen = fullscreen;
    toplevel->border_width = fullscreen ? 0 : border_width;
    toplevel_send_fullscreen(toplevel, toplevel->fullscreen);

    if (fullscreen) {
        toplevel->previous_geometry = toplevel->geometry;
        wlr_scene_node_reparent(&toplevel->scene->node, layers[FULLSCREEN]);
        toplevel_resize(toplevel, &toplevel->monitor->geometry, &toplevel->monitor->toplevel_space);
    } else {
        wlr_scene_node_reparent(&toplevel->scene->node, layers[toplevel->floating ? FLOATING : TILED]);
        toplevel_resize(toplevel, &toplevel->previous_geometry, &toplevel->monitor->toplevel_space);
        monitor_arrange(toplevel->monitor);
    }
}

void toplevel_set_monitor(struct toplevel *toplevel, struct monitor *monitor, uint32_t tags) {
    if (!toplevel || toplevel->monitor == monitor || !monitor) return;

    struct monitor *previous_monitor = toplevel->monitor;
    if (previous_monitor) {
        wl_list_remove(&toplevel->link);
        wl_list_remove(&toplevel->stack_link);
        // NOTE: I don't know if this is actually needed here.
        toplevel_focus(monitor_get_top(previous_monitor), true);
        monitor_arrange(previous_monitor);
    }

    toplevel->monitor = monitor;
    toplevel->tags = tags ? tags : toplevel->monitor->tagset[toplevel->monitor->sel_tags];
    wl_list_insert(&monitor->toplevels, &toplevel->link);
    wl_list_insert(&monitor->stack, &toplevel->stack_link);
    toplevel_set_fullscreen(toplevel, toplevel->fullscreen);
    toplevel_set_floating(toplevel, toplevel->floating);
    toplevel_focus(monitor_get_top(sel_mon), true);
    if (previous_monitor) toplevel_resize(toplevel, &toplevel->geometry, &toplevel->monitor->toplevel_space);
    if (!toplevel->fullscreen || !toplevel->floating) monitor_arrange(toplevel->monitor);
}

struct toplevel *toplevel_try_from_window(struct window *window) {
    if (!window || window->type != WINDOW_TOPLEVEL) return NULL;

    struct toplevel *toplevel = wl_container_of(window, toplevel, base);
    return toplevel;
}

void toplevel_unfocus(struct toplevel *toplevel) {
    if (!toplevel || !toplevel->monitor || toplevel != sel_mon->sel || toplevel_is_unmanaged(toplevel)) return;

    if (toplevel_is_xdg(toplevel)) {
        struct wlr_xdg_popup *popup, *tmp;
        wl_list_for_each_safe(popup, tmp, &toplevel->xdg.surface->popups, link)
            wlr_xdg_popup_destroy(popup);
    }

    toplevel_set_border(toplevel, colors[SCHEME_NORMAL][COLOR_BORDER]);
    toplevel_set_activated(toplevel, false);

    wlr_seat_keyboard_notify_clear_focus(seat);
}

void toplevel_unmap(struct wl_listener *listener, void *data) {
    struct toplevel *toplevel = wl_container_of(listener, toplevel, listeners.unmap);

    if (toplevel == toplevel_grab) toplevel_grab = NULL;

    wlr_scene_node_set_enabled(&toplevel->scene->node, false);

    if (toplevel_is_unmanaged(toplevel)) {
        if (&toplevel->base == exclusive_focus) {
            exclusive_focus = NULL;
            toplevel_focus(monitor_get_top(sel_mon), true);
            wlr_scene_node_raise_to_top(&sel_mon->sel->scene->node);
        }
        return;
    }

    wl_list_remove(&toplevel->link);
    wl_list_remove(&toplevel->stack_link);
    // TODO: Maybe just modify toplevel_set_monitor to handle this stuff.
    toplevel_focus(monitor_get_top(sel_mon), true);
    toplevel->monitor = NULL;
    toplevel->tags = 0;

    monitor_arrange(sel_mon);
}

bool toplevel_wants_floating(struct toplevel *toplevel) {
    if (!toplevel) return false;
    if (toplevel_is_x11(toplevel)) {
        struct wlr_xwayland_surface *surface = toplevel->x11.surface;
        if (toplevel->x11.surface->modal) return true;

        xcb_size_hints_t *size_hints = surface->size_hints;
        for (int i = 0; i < surface->window_type_len; i++) {
            if (surface->window_type[i] == netatom[NetWMWindowTypeDialog] ||
                    surface->window_type[i] == netatom[NetWMWindowTypeSplash] ||
                    surface->window_type[i] == netatom[NetWMWindowTypeToolbar] ||
                    surface->window_type[i] == netatom[NetWMWindowTypeUtility])
                return true;
        }

        return size_hints && size_hints->min_width > 0 &&
            size_hints->min_height > 0 &&
            (size_hints->max_width == size_hints->min_width ||
             size_hints->max_height == size_hints->min_height);
    }

    struct wlr_xdg_toplevel *top = toplevel->xdg.surface->toplevel;
	return top->parent ||
        (top->current.min_width != 0 &&
         top->current.min_height != 0 &&
         (top->current.min_width == top->current.max_width ||
          top->current.min_height == top->current.max_height));
}

void unlock(struct wl_listener *listener, void *data) {
    lock_destroy();
}

void view(const union arg *arg) {
    struct monitor *monitor = sel_mon;
    if (!monitor || (arg->ui & TAGMASK) == monitor->tagset[monitor->sel_tags]) return;

    monitor->sel_tags ^= 1;
    if (arg->ui & TAGMASK) monitor->tagset[monitor->sel_tags] = arg->ui & TAGMASK;

    toplevel_focus(monitor_get_top(sel_mon), true);
    monitor_arrange(monitor);
}

struct window *window_from_xy(uint32_t x, uint32_t y, double *nx, double *ny, struct wlr_surface **surface) {
    double unused;
    if (!nx || !ny) {
        nx = &unused;
        ny = &unused;
    }
    struct window *window = NULL;

    for (int32_t layer = NUM_LAYERS - 1; layer >= 0; layer--) {
        struct wlr_scene_node *node = wlr_scene_node_at(&layers[layer]->node, x, y, nx, ny);
        if (!node) continue;

        if (surface && node->type == WLR_SCENE_NODE_BUFFER) {
            struct wlr_scene_surface *scene = wlr_scene_surface_try_from_buffer(wlr_scene_buffer_from_node(node));
            if (scene) *surface = scene->surface;
        }
        for (struct wlr_scene_node *tmp = node; tmp && !window; tmp = tmp->parent ? &tmp->parent->node : NULL) {
            window = tmp->data;
        }
        if (window) break;
    }

    return window;
}

void xdg_activation_activate(struct wl_listener *listener, void *data) {
    struct wlr_xdg_activation_v1_request_activate_event *event = data;
    struct toplevel *toplevel = NULL;
    if (!client_from_surface(event->surface, NULL, NULL, &toplevel) || toplevel == monitor_get_top(sel_mon)) return;

    toplevel->urgent = true;
    toplevel_set_border(toplevel, colors[SCHEME_URGENT][COLOR_BORDER]);
}

xcb_atom_t xwayland_get_atom(xcb_connection_t *connection, const char *name) {
    if (!connection || !name) return 0;

    xcb_atom_t atom = 0;
    xcb_intern_atom_cookie_t cookie = xcb_intern_atom(connection, 0, strlen(name), name);
    xcb_intern_atom_reply_t *reply = xcb_intern_atom_reply(connection, cookie, NULL);
    if (reply) atom = reply->atom;
    free(reply);

    return atom;
}

void xwayland_ready(struct wl_listener *listener, void *data) {
    xcb_connection_t *connection = xcb_connect(xwayland->display_name, NULL);
    int32_t error = xcb_connection_has_error(connection);
    if (error) return;

	netatom[NetWMWindowTypeDialog]  = xwayland_get_atom(connection, "_NET_WM_WINDOW_TYPE_DIALOG");
	netatom[NetWMWindowTypeSplash]  = xwayland_get_atom(connection, "_NET_WM_WINDOW_TYPE_SPLASH");
	netatom[NetWMWindowTypeToolbar] = xwayland_get_atom(connection, "_NET_WM_WINDOW_TYPE_TOOLBAR");
	netatom[NetWMWindowTypeUtility] = xwayland_get_atom(connection, "_NET_WM_WINDOW_TYPE_UTILITY");

    wlr_xwayland_set_seat(xwayland, seat);

    struct wlr_xcursor *cursor = wlr_xcursor_manager_get_xcursor(xcursor, "default", 1);
    if (cursor && cursor->image_count > 0) {
        struct wlr_xcursor_image *image = cursor->images[0];
        wlr_xwayland_set_cursor(xwayland,
                image->buffer, image->width * 4,
                image->width, image->height,
                image->hotspot_x, image->hotspot_y);
    }

    xcb_disconnect(connection);
}

void zoom(const union arg *arg) {
    if (!sel_mon || !sel_mon->sel || sel_mon->sel->floating || !sel_mon->layouts[sel_mon->layout]->arrange || (sel_mon->sel->link.next == &sel_mon->toplevels && sel_mon->sel->link.prev == &sel_mon->toplevels)) return;

    wl_list_remove(&sel_mon->sel->link);
    wl_list_insert(&sel_mon->toplevels, &sel_mon->sel->link);

    monitor_arrange(sel_mon);
}

int main(int argc, char **argv) {
    wlr_log_init(WLR_SILENT, NULL);

    setup();
    run();
    cleanup();

    return EXIT_SUCCESS;
}

void _panic(uint32_t line, const char *file, const char *fmt, ...) {
    printf("[%u][%s][yutani][panic] ", line, file);

    va_list list;
    va_start(list, fmt);
    vfprintf(stderr, fmt, list);
    va_end(list);
    fputc('\n', stderr);

    exit(EXIT_FAILURE);
}

void *ecalloc(size_t blocks, size_t size) {
    void *ptr = calloc(blocks, size);
    // NOTE: I don't really like this because it makes the panic a little pointless as it will
    // always show the line inside this function not the caller's line.
    //
    // TODO: Maybe wrap this function in a macro like with _panic? Then have it pass in the line we are on?
    // Seems hacky and a little stupid to me though. But like within boundaries.
    if (!ptr) panic("Allocation failed");
    return ptr;
}

void *ezalloc(size_t size) {
    return ecalloc(1, size);
}
