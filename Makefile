VERSION 	  := 0.0
PREFIX		  := /usr/local
SRC 		  := src
INCLUDE 	  := include
PROTOCOLS_IN  := protocols
PROTOCOLS_OUT := ${INCLUDE}/protocols

PKG_CONFIG 			:= pkg-config
WAYLAND_SCANNER 	:= `${PKG_CONFIG} --variable=wayland_scanner wayland-scanner`
WAYLAND_PROTOCOLS 	:= `${PKG_CONFIG} --variable=pkgdatadir wayland-protocols`
PKGS 				:= wlroots wayland-server xkbcommon libinput xcb xcb-icccm fcft pixman-1 libdrm
override CFLAGS  	:= ${CFLAGS} `${PKG_CONFIG} --cflags ${PKGS}` -Wall -DWLR_USE_UNSTABLE -DVERSION=\"${VERSION}\" -I./include/ -I./include/protocols/
override LIBS		:= ${LIBS} `${PKG_CONFIG} --libs ${PKGS}` -lm

PROTOCOLS			:= ${PROTOCOLS_OUT}/xdg-shell-protocol.h ${PROTOCOLS_OUT}/wlr-layer-shell-unstable-v1-protocol.h ${PROTOCOLS_OUT}/cursor-shape-v1-protocol.h ${PROTOCOLS_OUT}/keyboard-shortcuts-inhibit-protocol.h ${PROTOCOLS_OUT}/pointer-constraints-unstable-v1-protocol.h
INCLUDES 			:= ${INCLUDE}/window.h ${INCLUDE}/config.h ${INCLUDE}/client.h ${INCLUDE}/buffer.h ${INCLUDE}/swapchain.h
SRCS				:= ${SRC}/main.c ${SRC}/client.c ${SRC}/buffer.c ${SRC}/swapchain.c

all: yutani

yutani: ${SRCS} ${INCLUDES} ${PROTOCOLS_OUT} ${PROTOCOLS}
	${CC} -o $@ ${SRCS} ${CFLAGS} ${LIBS}

install: all
	mkdir -p ${DEST_DIR}${PREFIX}/bin
	cp -f yutani ${DEST_DIR}${PREFIX}/bin
	chmod 755 ${DEST_DIR}${PREFIX}/bin/yutani

uninstall:
	rm -rf ${DEST_DIR}${PREFIX}/bin/yutani

dist: clean
	mkdir -p yutani-${VERSION}
	cp -r Makefile ${INCLUDE} ${SRC} ${PROTOCOLS_IN} README.md yutani-${VERSION}
	tar -cf yutani-${VERSION}.tar yutani-${VERSION}
	rm -rf yutani-${VERSION}

${PROTOCOLS_OUT}:
	mkdir -p $@
${PROTOCOLS_OUT}/xdg-shell-protocol.h:
	${WAYLAND_SCANNER} server-header ${WAYLAND_PROTOCOLS}/stable/xdg-shell/xdg-shell.xml $@
${PROTOCOLS_OUT}/keyboard-shortcuts-inhibit-protocol.h:
	${WAYLAND_SCANNER} server-header ${WAYLAND_PROTOCOLS}/unstable/keyboard-shortcuts-inhibit/keyboard-shortcuts-inhibit-unstable-v1.xml $@
${PROTOCOLS_OUT}/pointer-constraints-unstable-v1-protocol.h:
	${WAYLAND_SCANNER} server-header ${WAYLAND_PROTOCOLS}/unstable/pointer-constraints/pointer-constraints-unstable-v1.xml $@
${PROTOCOLS_OUT}/cursor-shape-v1-protocol.h:
	${WAYLAND_SCANNER} server-header ${WAYLAND_PROTOCOLS}/staging/cursor-shape/cursor-shape-v1.xml $@
${PROTOCOLS_OUT}/wlr-layer-shell-unstable-v1-protocol.h:
	${WAYLAND_SCANNER} server-header ${PROTOCOLS_IN}/wlr-layer-shell-unstable-v1.xml $@

${INCLUDE}/config.h:
	cp ${INCLUDE}/config.def.h $@

clean:
	rm -rf ${INCLUDE}/config.h ${PROTOCOLS_OUT} ./yutani

debug: CFLAGS += -fsanitize=address,undefined,bounds,enum,leak -g
debug: dev yutani
	./yutani 2> log.txt

dev: clean ${INCLUDES} ${PROTOCOLS_OUT} ${PROTOCOLS}
